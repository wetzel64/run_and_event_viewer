#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Aug 27 10:02:19 2019
@author: michal
"""
import pickle
import numpy as np
import rosahami_processor as rosa
import matplotlib.pyplot as plt
import rossendorfer_farbenliste as rofl
import sys, os

def get_rosahami_unwrapped_ff_image(image, flatfield = "rosahami-unwrap-map-XFEL", verbose=False):
    """
        **This function unwrapps and flatfields the data for the SAXS detector**
        
        :param image: (*np.ndarray*) - input raw image
        :param flatfield: (*str*) or None - provide a valid filename for the flatfield to apply. The file is looked for in subdirectory ``rosahami`` to do the flatfield correction, e.g. ``0211-JF4_mean_flatfield`` for ``./rosahami/0211-JF4_mean_flatfield.pickle``
        :param verbose: (*bool*) *optional* - True (default) to plot intermediate steps
        
        returns::
            (*np.ndarray*) unwrapped and flatfielded image, (*np.ndarray*) - qx axis, (*np.ndarray*) - qy axis
    """
    if verbose:
        print("================= Welcome at rosahami (c) michal ! =================")
    else:
        stdout = sys.stdout#__stdout__
        sys.stdout = open(os.devnull, "w")
    try:
        max_qX=1. # values for interpolation of the resulting data
        step_qX=0.002
        max_qY=1.
        step_qY=0.002
        if verbose:
            print("Attention! This is an alpha version. The q-axis resolution, rotation and range are hardcoded")

        flatfield_pickle='rosahami/'+flatfield+'.pickle' #flat field & geometry to be used. This file will be generated based on scattering on nanospheres, most likely by Michal
        rotation_deg=31.5 #rotation of the data to make the scattering horizontal

        ## Load & orient the data
        (flatfield,p)=pickle.load(open(flatfield_pickle, "rb" ) ) # loading the configuration for given setup
        interpAlX,interpAlY = rosa.load_unwrap_map(p['unwrap_map'])
        image = rosa.remove_jungfrau_stripes(image) # get rid of the detector artefacts
        image = np.transpose(image)  # just do this. It make sense for the typically used geometry

        if verbose:
            print('doing the unwrapping, this might take some time...')
        points = rosa.do_unwrap(image,p,interpAlX,interpAlY) #Geometry correction - this is the big thing!!!
        if verbose:
            print('...done!')

        ff = flatfield[4,:]    #flatfield loaded from the pickle
        points[5,:]=ff #storing the flatfield into the points field
        points[6,:]=points[2,:]/ff #calculating the flatfielded values
        points = rosa.rotate(points,rotation_deg) # Rotation, just to have the scattering horizontal
        #pickle.dump(points,open( './points/SAXS_points_'+dataname+'.pickle', "wb" )) # saving for later use

        xqi,yqi,data,ff,flatfielded = rosa.interpolate_scattering_q(points,max_qX,step_qX,max_qY,step_qY,XFEL_photon_energy=p['xfel_energy']) # doing the interpolation in q space
    #    data = np.transpose(data)  # just do this. It make sense for the typically used geometry
    #    flatfielded = np.transpose(flatfielded)  # just do this. It make sense for the typically used geometry
    #    ff = np.transpose(ff)  # just do this. It make sense for the typically used geometry

        if verbose: # drawing            
            nx=points[0,:]*1e3 #the 2ThetaX and 2ThetaY values in [mrad]
            ny=points[1,:]*1e3
            plt.figure()
            plt.scatter(nx,ny,c=np.log10(points[2,:]),s=4,cmap=rofl.cmap())
            plt.colorbar()
            plt.clim(-3,1)
            plt.xlabel(r'Horizontal scattering angle 2$\theta$ [mrad]')
            plt.ylabel(r'Vertical scattering angle 2$\theta$ [mrad]')
            plt.title("Reconstructed scattering (logarithmic colorscale)")

            plt.figure()
            plt.pcolor(xqi,yqi,np.log10(data),cmap=rofl.cmap())
            plt.colorbar()
            plt.clim(-3,1)
            plt.xlabel(r'Horizontal Q [nm-1]')
            plt.ylabel(r'Vertical Q [nm-1]')
            plt.title("Reconstructed interpolated scattering (logarithmic colorscale)")

            plt.figure()
            plt.pcolor(xqi,yqi,np.log10(flatfielded),cmap=rofl.cmap())
            plt.colorbar()
            plt.clim(-3,1)
            plt.xlabel(r'Horizontal Q [nm-1]')
            plt.ylabel(r'Vertical Q [nm-1]')
            plt.title("Flatfielded interpolated scattering (logarithmic colorscale)")        

        return_set = flatfielded
        return_set = np.transpose(return_set)
        min_ = np.min(return_set[np.invert(np.isnan(return_set))])
        return_set[np.isnan(return_set)] = min_
        if verbose:
            print("Rosahami is done!")
        else:
            sys.stdout = stdout        
    except Exception as e:
        raise
        if verbose:
            print(e)
            print("Rosahami didn't work!")
        else:
            sys.stdout = stdout
            print(e)
    return return_set, xqi[0,:], yqi[:,0], rotation_deg
    #return flatfielded, xqi, yqi, rotation_deg
           