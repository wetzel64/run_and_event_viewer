import sys
import random
import matplotlib
import os
from copy import copy, deepcopy
try:
    import imagej
    import scyjava
except Exception as e:
    print("Warning: Could not import the imagej module. ImageJ will not be available.")
global ij_handler
ij_handler = None
from skimage.transform import rotate
from skimage.transform import resize
from skimage import data
from scipy.ndimage import gaussian_filter
from pathlib import Path
import paramiko
#matplotlib.use('Qt6Agg')
#%matplotlib qt
#from matplotlib.backends.qt_compat import QtWidgets
#try:
#    import PyQt6
#    from PyQt6 import QtCore, QtWidgets, QtGui
#    from PyQt6.QtGui import QAction as QAction
#except Exception as e:
#    print("PyQt6 not found. GUI will not be available!")
#from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg, NavigationToolbar2QT as NavigationToolbar
#from matplotlib.backends.backend_qt5agg import FigureCanvas as FigureCanvas
from mpl_toolkits.axes_grid1.inset_locator import inset_axes
from matplotlib.figure import Figure
import matplotlib.pyplot as plt
from mpl_toolkits.axes_grid1 import make_axes_locatable
import matplotlib as mpl
import numpy as np
import tifffile
import getpass
#for google spreadsheet meta data extraction:
import gspread
import pandas as pd
from oauth2client.service_account import ServiceAccountCredentials
from slack_sdk import WebClient
slack_client = None

import functools
from scipy import ndimage
import rossendorfer_farbenliste as rofl
mpl.cm.register_cmap(cmap=rofl.cmap(),name="rofl")

#try:
import review_rosahami_connector as rosa
rosa_connected = True
#except:
#    rosa_connected = False
#mpl.colormaps.register(rofl.cmap(),name="rofl")
#colors2 = rofl.cmap_nw()(np.linspace(0,1,total_event_no))

#sys.stdout = sys.__stdout__



try:
    import extra_mmm_new as em
    #import importlib
    #importlib.reload(em)
except Exception as e:
    print("Warning: extra_data resources not found. Source 'API' will not be available!")

matplotlib.rcParams["figure.autolayout"] = True


def find_all(a_str, sub):
    list_return=[]
    start = 0
    while True:
        start = a_str.find(sub, start)
        if start == -1: return list_return
        list_return.append(start)
        start += len(sub) # use start += 1 to find overlapping matches
    
#----------------------------- 
class SSH():
    """
        **Control the connection to an external SSH server**
        
        This class allows etsablishes a connection to an external SSH server to look for new runs and events, and to download the images if new are existing
                
        - Used by :class:`.Experiment`
    """
    def __init__(self,hostname: str,username = None,password=None,verbose=False):
        """
            **Create the new ssh and sftp clients**

            This class allows etsablishes a connection to an external SSH server to look for new runs and events, and to download the images if new are existing

            If no connection could be established, password will be asked for again. This could end in an infinite loop. 
            If so, check your hostname and username. 
            Currently only password authentication is supported.

            :param hostname: The ssh hostname of the server, e.g. ``'server.address.com'``
            :param username: The username that will be used to log in to the server.
            :param password: optional - If not provided, the user will get a prompt upon the first connection attempt
            :param verbose: *bool*, enable/disable diagnostic output.

            - Example::
                ``ssh = SSH(hostname: *str*, username: *str*, password = None: *str* or *None*, verbose = False: *bool*)``

            - Expected Success Response::
                returns an object that handles the file lookup and download from the remote server

            - Used by :class:`.Experiment`
            
        """
        self.client = paramiko.SSHClient()
        self.hostname=hostname
        self.username=username
        self.password = password
        if self.username == None: 
            self.username = input("Enter the username for " + self.hostname+":")
        if self.password == None: 
            self.password = getpass.getpass("Enter the password for "+self.username+"@"+self.hostname+":")
        self.client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
                
        error = 1
        while error == 1:
            try:
                self.client.connect(hostname, username=self.username, password=self.password, timeout=5)
                error = 0
                continue
            except Exception as e:
                username = getpass.getpass("Could not connect. Re-enter the username for " + self.hostname+":")
                password = getpass.getpass("Re-enter the password for "+self.username+"@"+self.hostname+":")
                error = 1
        try:
            self.sftp = self.client.open_sftp()
        except Exception as e:
            self.statusbar.showMessage("Error! Could not connect the SFTP server!")
            self.statusbar.showMessage(e)
            if self.debug: raise

    def get_file_list(self,checkPattern: str,verbose=False):
        """
            **Get the list of files at the remote server**
            
            This function uses the ssh connection to ``ls [checkPattern]`` on a remote server.
            
            :param checkPattern: *str* defining the mask for ``ls`` to list files, e.g. ``'/home/[user]/review/data/*.tif'``
            :param verbose: *bool*, enable/disable diagnostic output.
                
            - Example::
                ``get_file_list(checkPattern: *str*, verbose = True: *bool*)``

            - Expected Succes Response::
                returns a *list* of *str* that contain the matching files at the remote server
                
            - Expected Error Response::
                empty *list* ``[]`` 
        """
        try:
            (stdin, stdout, stderr) = self.client.exec_command("ls "+checkPattern)
            return "{}".format(stdout.read())[2:-3].split('\\n')
        except Exception as e:
            []
            
    def get_file(self, remote="", local="", verbose=False):
        """
            **Download file from SFTP**
            
            This function uses the ssh connection to ``ls [checkPattern]`` on a remote server.
            
            :param remote: *str* of the remote file path where the file will be downloaded from, incl. filename
            :param local: *str* of the local file path where the file will be downloaded to, incl. filename
            :param verbose: *bool*, enable/disable diagnostic output.
                
            - Example::
                ``get_file(remote: *str*, local: *str*, verbose = True: *bool*)``

            - Expected Succes Response::
                ``True``
                
            - Expected Error Response::
                ``False``
        """
        try:
            #self.statusbar.showMessage("Downloading file ",remote)
            Path(local[0:find_all(local,'/')[-1]]).mkdir(parents=True, exist_ok=True) #os.sep
        except Exception as e:
            #self.statusbar.showMessage("Error: Local directory does not exist and could not be created! ")
            #self.statusbar.showMessage(e)
            #if self.debug: raise
            return False
        try:
            self.sftp.get(remote, local)
            return True
        except Exception as e:
            #self.statusbar.showMessage("Error! SFTP connection could not be established")
            #self.statusbar.showMessage(e)
            #if self.debug: raise
            return False

class Experiment():
    """
        **Holds a list of runs and can perform batch operations on them.**
        This is the main class that holds the runs and events data, and can later also connect to a GUI
    """
    
    def __init__( self, main_window=None, runs = {}, settings={}, metadata = None, verbose=False,debug=False):
        """
            **Create the Experiment object**
            
            This is the main class that holds the runs and events data, and can later also connect to a GUI

            :param main_window: pointer to the MainWindow class :class:`.MainWindow`. This is used for interaction with the statusbar, new run/event notification
            :param settings: *dict* of global settings. Can be obtained from the GUI :class:`.SettingsWindow`. :ref:`More information<settings>`.
            :param verbose: *bool*, enable/disable diagnostics output

            - Example:: 
                ``Experiment( main_window=None, runs = {}, settings={}, verbose=False )``
                  
        """
        self.debug=debug
        if main_window != None:
            try:
                self.settings = main_window.settings
                """*dict* of settings that are required to configure the local parameters neccessary to run the program. :ref:`More information<settings>`."""
            except:
                None
            try:
                self.statusbar = main_window.statusbar
                """*pointer* to StatusBar object, that handles status outputs."""
            except:
                class Statusbar():
                    def __init__(self):
                        None
                    def showMessage(self,text):
                        if verbose:
                            print(text)
                        else:
                            None
                self.statusbar = Statusbar()

            try:
                self.main_window = main_window
                """*pointer* to object of :class:`.MainWindow`. Used to notify of new events by calling the functions :func:`~review.MainWindow.new_run` and :func:`~review.MainWindow.new_run`, and used to output information in :class:'.MainWindow' statusbar"""
            except:
                None
        else:
            class Statusbar():
                def __init__(self):
                    None
                def showMessage(self,text):
                    if verbose:
                        print(text)
                    else:
                        None
            self.statusbar = Statusbar()
            
        self.runs=runs # holds all the runs
        """*dict* with keys of run_numbers and items of objects of :class:`.Run`"""
        self.cached = [] # run_numbers of cached runs
        """*list* with run_numbers of those runs that are loaded and images are in local memory cache."""
        self.last_run_read = 0
        """*int* of the run that was found last. Can be used to display the last run."""
        self.last_event_read = 0
        """*int* of the event that was found last. Can be used to display the last event together with ``last_run_read``."""
        self.latest_events = {}
        """
            *dict* with keys of the run_numbers, items are a list with the newly event_numbers.

            Stores the events that were found in the last call of :meth:`~update`.

            .. note::
                As the update function might be called often and this variable might be overwritten, do not use tis variable to track all the existing runs and events! Rather, use the :meth:`~get_run_numbers` and :meth:`Run.get_event_numbers` methods for this purpose!
            """
        
        self.settings = settings
        self.metadata = metadata
        if settings == {}:
            #set standard settings
            self.settings["filePrefix"] = '/run'
            self.settings["localDataPath"] = "data/"
            self.settings["fileSeparator"] = ""
            self.settings["fileType"] = "npz"
            self.settings["fileEnd"] = "-"
            self.settings["fileMustHave"] = "JF4_mean"
            self.settings["detector"] = "JF4"
            self.settings["local"] = True
            self.settings["API"] = False
            self.settings["SFTP"] = False
            self.settings["cache"] = True
            self.settings["cacheMemory"] = -1
            self.settings["hostname"] = "max-display.desy.de"
            self.settings["username"] = None
            self.settings["password"] = None
            self.settings["remoteDataPath"] = "/gpfs/exfel/exp/HED/202101/p002621/usr/Software/Thomas/data/viewer"            
            
    #def set_source(self,source):
    #    """Helper method for easy switching between  XFEL/Maxwell on-site API usage and remote/local usage."""
    #    if source == "API" and self.settings["API"] == False:
    #        self._old_local = settings["localDataPath"]
    #        self._old_output = settings["localOutputPath"]
    #        settings["localDataPath"] = settings["remoteDataPath"]
    #        settings["localOutputPath"] = settings["remoteOutputPath"]
    #    if source != "API":
    #        try:
    #            settings["localDataPath"] = self._old_local
    #            settings["localOutputPath"] = self._old_output
    #        except Exception as e:
    #            None#

    #    self.settings["local"] = False
    #    self.settings["SFTP"] = False
    #    self.settings["API"] = False
    #    self.settings[source] = True
            
    def set_center(self,event=None):
        if event == None:
            run = self[-1]
            event = run[-1]
        event.set_center()
         
    def savefig(self,directory,fn):
        root = self.settings["localOutputPath"]
        Path(root[0:find_all(root,'/')[-1]]).mkdir(parents=True, exist_ok=True)
        path = root+"/"+directory+"/"
        Path(root[0:find_all(root,'/')[-1]]).mkdir(parents=True, exist_ok=True)
        plt.savefig(path+fn+".jpg")
    
    def savefigs(self,directory,fn,figs,slack=False,run_number=0):
        import sys
        from PIL import Image

        root = self.settings["localOutputPath"]
        Path(root[0:find_all(root,'/')[-1]]).mkdir(parents=True, exist_ok=True)
        path = root+"/"+directory+"/"
        Path(root[0:find_all(root,'/')[-1]]).mkdir(parents=True, exist_ok=True)

        images=[]
        for fn_ in figs:
            try:
                images.append(Image.open(root+"/events/"+fn_+".jpg"))
            except:
                images.append(Image.open(root+"/runs/"+fn_+".jpg"))
        widths, heights = zip(*(i.size for i in images))

        width = max(widths)
        height =np.sum(heights)

        new_im = Image.new('RGB', (width, height))

        x_offset = 0
        y_offset = 0
        for image in images:
            new_im.paste(image, (x_offset,y_offset))
            y_offset += image.size[1]
        new_im.save(path+fn+'.jpg')
        if slack != False:
            self.send_fig_slack("overviewbot", run_number = run_number, fn = path+fn+'.jpg')


    def plot_overview(self,detectors=[''],last_event_only=False,linewidth=16,smooth=2,sort_by="delay",bg_subtraction=True,latest_events_only = False,slack=False,limit=0,norm="max",savefig=False,skip_existing=False,processed=True,recalc = False):
        """
            **Plot an overview of all events and runs**
            
            This is a plotting method using ``matplotlib``. It plots the detector 2d image of the standard diognostics, or all the detectors given by parameter `detectors`` next to each other, labeling with metadata using the class :class:`.Metadata`. 
            It is quite specific for a RELAX experiment at the moment. We assume **JF4 is the SAXS detector**, used for background correction by default, and adding the ``1/q^2`` reference line. 
            
            :param bg_subtraction: (*bool*), default: ``True``. Enable background subtraction for the SAXS detector. 
            :param detectors: (*str*) Detectors that shall be included in the overview. By default only the default detector specified in the settings is displayed.
            :param last_event_only: (*bool*), default: ``True``. Plots only the camera image of the last event (lineouts for all runs and events)
            :param linewidth: (*int*) Thickness of the line of the profile over which to average.
            :param smooth: (*float*) Gaussian_filter width
            :param sort_by: (*str*) metadata key used for sorting the lineouts by color
            :param slack: (*str*) or ``None`` or (*bool*) Send graph to Slack, if not ``False``. If not ``None``, specify a *str* for channel name, else standard "slackbot" will be used.
            :param limit: *int* Only plot the last few runs. 0 for all.
            :param lates_events_only: (*bool*) Only plot the runs and events stored in :attr:`~latest_events`
            
            - Example:: 
                This function can be easily used to plot only a subset, also. E.g. by filtering for mainshots only, that have a delay between 20 ps and 30 ps: 

                .. code-block:: python

                   filtered = Experiment(runs={k:v for k,v in experiment.runs["mainshots"].items() if v.get_metadata("delay") != None and v.get_metadata("delay") > 20}) # filter for certain criteria
                   filtered.plot_overview()
                   
                See also :func:`~plot_profiles`, :func:`~plot_detectors`, :meth:`Run.plot_overview`, :meth:`Run.plot_detectors`, :meth:`Run.plot_profiles`, :meth:`Event.plot_overview`, :meth:`Event.plot_detectors`, :meth:`Event.plot_profile`, :meth:`Event.plot_profiles`. More information can also be found in the :ref:`plotting section<Plotting>`. 
                   
            .. note::
            
                This plotting method uses the post-processed image data optained from :func:`~review.Event.get_processed_image`. This uses metadata in the google labbook to rotate, correct perspective, darkfielding, flatfielding etc.

            .. note::
            
                This plotting method is very specific w.r.t. background subtraction. This needs to be abstracted!
                  
        """
        try:
            limit=np.min([len(self.runs),limit])
            if latest_events_only:
                limit_=limit
                limit = np.min([len(self.latest_events),limit])
                for run_number in list(self.latest_events.keys())[-limit:]:
                    figs=[]
                    for event_number in self.latest_events[run_number]:
                        try:
                            ev = self[run_number][event_number]
                            figs.append(
                                ev.plot_overview(detectors=detectors,linewidth=linewidth,smooth=smooth,bg_subtraction=bg_subtraction,slack=slack,norm="",savefig=savefig,processed=processed,recalc=recalc)
                            )
                            if ev.shot_type == "mainshot":
                                try:
                                    for direction in ["horizontal","vertical"]:
                                        figs.append(
                                            self[run_number].plot_profiles(detectors=detectors,direction=direction,
                                                                                  linewidth=linewidth,
                                                                                  smooth=smooth,
                                                                                  bg_subtraction=bg_subtraction,
                                                                                  slack=slack,
                                                                                  norm=norm,
                                                                                  processed=processed,
                                                                                  recalc = recalc,
                                                                                  savefig=savefig)
                                        )
                                    fn = self.settings["filePrefix"] + str(run_number) + self.settings["fileEnd"] + "overview"
                                    self.savefigs("runs",fn,figs,slack=slack,run_number=run_number)        

                                    try:
                                        last_runs = self[-limit_:]
                                        for direction in ["horizontal","vertical"]:
                                            last_runs["mainshots"].plot_profiles(detectors = detectors,direction=direction,
                                                                                        linewidth=linewidth,
                                                                                        sort_by=sort_by,
                                                                                        smooth=smooth,
                                                                                        limit=10,
                                                                                        bg_subtraction=bg_subtraction,
                                                                                        slack=slack,
                                                                                        processed=processed,
                                                                                        norm=norm,
                                                                                        recalc = recalc
                                                                                       )
                                    except Exception as e: 
                                        if self.debug: raise
                                except Exception as e: 
                                    if self.debug: raise
                        except Exception as e: 
                            if self.debug: raise
            else:
                for run in self[-limit:]:
                    try:
                        run.plot_overview(detectors=detectors, last_event_only=last_event_only,linewidth=linewidth,smooth=smooth,bg_subtraction=bg_subtraction,slack=slack,norm=norm,savefig=savefig,processed=processed,recalc=recalc)
                            #run.plot_horizontal_profiles(detectors = detectors,linewidth=linewidth,smooth=smooth,bg_subtraction=bg_subtraction)
                    except Exception as e:
                        try:
                            run.plot_overview(detectors=detectors,linewidth=linewidth,smooth=smooth,bg_subtraction=bg_subtraction,slack=slack,norm=norm,savefig=savefig,processed=processed,recalc=recalc)
                        except Exception as e:
                            self.statusbar.showMessage(e)

                # horizontal profiles------------------------------------------------------
                try:
                    if len(self.runs) > 1 and not last_event_only:
                        temp=self[-limit:]
                        for direction in ["horizontal","vertical"]:
                            temp["mainshots"].plot_profiles(detectors = detectors,direction=direction,
                                                                   linewidth=linewidth,
                                                                   sort_by=sort_by,
                                                                   smooth=smooth,
                                                                   bg_subtraction=bg_subtraction,
                                                                   slack=slack,
                                                                   processed=processed,
                                                                   recalc=recalc,
                                                                   norm=norm
                                                                  )
                except Exception as e:
                    self.statusbar.showMessage(e)
                    if self.debug: raise
        except Exception as e:
            self.statusbar.showMessage(e)
            if self.debug: raise
                
    def plot_detectors(self,detectors=[''],last_event_only=True,linewidth=16,smooth=2,sort_by="delay",bg_subtraction=True,slack=False,limit=0,norm="max",processed=True,recalc=False):
        """
            **Plot an overview of all events and runs**
            
            This is a plotting method using ``matplotlib``. It plots the detector 2d image of the standard diognostics, or all the detectors given by parameter `detectors`` next to each other, labeling with metadata using the class :class:`.Metadata`. 
            It is quite specific for a RELAX experiment at the moment. We assume **JF4 is the SAXS detector**, used for background correction by default, and adding the ``1/q^2`` reference line. 
            
            :param bg_subtraction: (*bool*), default: ``True``. Enable background subtraction for the SAXS detector. 
            :param detectors: (*str*) Detectors that shall be included in the overview. By default only the default detector specified in the settings is displayed.
            :param last_event_only: (*bool*) default: ``True``. Plots only the camera image of the last event (lineouts for all runs and events)
            :param linewidth: (*int*) Thickness of the line of the profile over which to average.
            :param smooth: (*float*) Gaussian_filter width
            :param sort_by: (*str*) metadata key used for sorting the lineouts by color
            :param slack: (*str*) or ``None`` or (*bool*) Send graph to Slack, if not ``False``. If not ``None``, specify a *str* for channel name, else standard "slackbot" will be used.
            :param limit: *int* Only plot the last few runs. 0 for all.
            
            Example::
                This function can be easily used to plot only a subset, also. E.g. by filtering for mainshots only, that have a delay between 20 ps and 30 ps: 

                .. code-block:: python

                   filtered = Experiment(runs={k:v for k,v in experiment.runs["mainshots"].items() if v.get_metadata("delay") != None and v.get_metadata("delay") > 20}) # filter for certain criteria
                   filtered.plot_overview()
                   
                See also :func:`~plot_profiles`, :func:`~plot_overview`, :meth:`Run.plot_overview`, :meth:`Run.plot_detectors`, :meth:`Run.plot_profiles`, :meth:`Event.plot_overview`, :meth:`Event.plot_detectors`, :meth:`Event.plot_profile`, :meth:`Event.plot_profiles`. More information can also be found in the :ref:`plotting section<Plotting>`. 
                   
            .. note::
            
                This plotting method uses the post-processed image data optained from :func:`~review.Event.get_processed_image`. This uses metadata in the google labbook to rotate, correct perspective, darkfielding, flatfielding etc.

            .. note::
            
                This plotting method is very specific w.r.t. background subtraction. This needs to be abstracted!
                  
        """
        limit = np.min([len(self.runs),limit])
        for run in self[-limit:]:
            run.plot_detectors(detectors=detectors,last_event_only=last_event_only,linewidth=linewidth,smooth=smooth,slack=slack,norm=norm,processed=processed,recalc=recalc)
            
    def plot_profiles(self,direction="horizontal",detectors=[''],last_event_only=False,sort_by="delay",linewidth=16,smooth=2,bg_subtraction=True,slack=False,limit=0,norm="max",savefig=False,add_q2=False,processed=True,recalc=False):
        """
            **Plot the horizontal profiles of all events and runs in one graph**
            
            This is a plotting method using ``matplotlib``. It plots the detector 2d image horizontal profile of the standard diognostics, or of all the detectors given by parameter `detectors`` next to each other, labeling with metadata using the class :class:`.Metadata`. 
            It is quite specific for a RELAX experiment at the moment. We assume **JF4 is the SAXS detector**, used for background correction by default, and adding the ``1/q^2`` reference line. 
            
            :param bg_subtraction: *bool*, default: ``True``. Enable background subtraction for the SAXS detector. 
            :param detectors: *str* Detectors that shall be included in the overview. By default only the default detector specified in the settings is displayed.
            :param last_event_only: *bool*, default: ``True``. Plots only the camera image of the last event (lineouts for all runs and events)
            :param linewidth: *int* Thickness of the line of the profile over which to average.
            :param smooth: *float* Gaussian_filter width
            :param sort_by: *str* metadata key used for sorting the lineouts by color
            :param slack: (*str*) or ``None`` or (*bool*) Send graph to Slack, if not ``False``. If not ``None``, specify a *str* for channel name, else standard "slackbot" will be used.
            :param limit: *int* Only plot the last few runs. 0 for all.

            Example:: 
                This function can be easily used to plot only a subset (sorted by delay by default), also. E.g. by filtering for mainshots only, that have a delay between 20 ps: 

                .. code-block:: python

                   filtered = Experiment(runs={k:v for k,v in experiment.runs["mainshots"].items() if v.get_metadata("delay") != None and v.get_metadata("delay") > 20}) # filter for certain criteria
                   filtered.plot_profiles() 
                   
                See also :func:`~plot_detectors`, :func:`~plot_overview`, :meth:`Run.plot_overview`, :meth:`Run.plot_detectors`, :meth:`Run.plot_profiles`, :meth:`Event.plot_overview`, :meth:`Event.plot_detectors`, :meth:`Event.plot_profile`, :meth:`Event.plot_profiles`. More information can also be found in the :ref:`plotting section<Plotting>`. 
                   
            .. note::
            
                This plotting method uses the post-processed image data optained from  
                :func:`~review.Event.get_processed_image`. This uses metadata in the google labbook 
                to rotate, correct perspective, darkfielding, flatfielding etc.

            .. note:: 
                This plotting method is very specific w.r.t. background subtraction. 
                This needs to be abstracted!
                                  
        """
        colors=["green","blue","red","black"]
        total_event_no=0
        for run in self[-limit:]:
            for ev in run:
                total_event_no+=1
                
        if sort_by != None:
            # sort
            try:
                runs2plot = dict(sorted(self[-limit:].get_metadatas(sort_by).items(), key=lambda item: item[1]))
            except Exception as e:
                if self.debug: raise
                self.statusbar.showMessage(e)
                if self.debug: raise
        else:
            #unsorted
            runs2plot = self.runs

        colors2 = rofl.cmap_nw()(np.linspace(0,1,total_event_no))

        fig, ax = plt.subplots(nrows=1, ncols=len(detectors),figsize=(11*len(detectors),6))
        if not (isinstance(ax,list) or isinstance(ax,np.ndarray)): ax=[ax]
        for j_, detector in enumerate(detectors):
            if isinstance(norm,str): 
                norm_ = norm
            else:
                norm_ = norm[j_]

            if detector == '': detector = self.settings["detector"]

            if isinstance(smooth,(list,np.ndarray)):
                smooth_=smooth[j_]
            else:
                smooth_=smooth
            if isinstance(linewidth,list) or isinstance(linewidth,np.ndarray): 
                linewidth_ = linewidth[j_]
            else:
                linewidth_ = linewidth

            plots = 0
            min_=1e99
            max_=0
            max__=0
            for k_,run_number in enumerate(list(runs2plot.keys())):
                run = self.runs[run_number]
                for i_, ev in enumerate(run):
                    if len(run.events)==1:
                        color = colors2[k_]
                        draw_background = False
                    else:
                        if ev.shot_type == "preshot": color=colors[i_ % 2]
                        if ev.shot_type == "mainshot": color="red"
                        if ev.shot_type == "postshot": color="black"
                        if detector == "JF4": 
                            if i_==0: add_q2=True*add_q2
                            draw_background = True*add_q2
                        else:
                            bg_subtraction = False
                            draw_background = False

                    plots += 1
                    if plots <= limit or limit <= 0:
                        min_,max_=ev.plot_profile(detector,direction=direction,
                                                  ax=ax[j_],
                                                  min_=min_,max_=max_,
                                                  color=color,
                                                  linewidth=linewidth_,
                                                  add_q2=add_q2,
                                                  smooth=smooth_,
                                                  bg_subtraction=bg_subtraction,
                                                  draw_background=draw_background,
                                                  norm=norm_,
                                                  processed=processed,
                                                  savefig = savefig,
                                                  recalc=recalc
                                                )
            if detector == "JF4": 
                min_ = 0.00125 * max_
                max_ = 1.6*max_
            ax[j_].set_ylim([0.75*min_,max_*1.25])
            #ax[j_].set_xlim([np.min(np.where(lh>0)),np.max(np.where(lh>0))])
            if j_ == 0: ax[j_].legend()
            ax[j_].set_title(detector+' | bg sub: ' + str(bg_subtraction))
        fig.suptitle(direction+" profiles")
        plt.tight_layout()

        if slack != False:
            self.send_fig_slack(slack)

        fn = self.settings["filePrefix"] + "all" + self.settings["fileEnd"] + direction+"Profile"
        if savefig:
            self.runs.savefig("events",fn)

        plt.show()
        return fn
                
    def send_fig_slack(self,channel, run_number=None, event_number=None,fn=None):
        """
            **Send a figure to slack**
            
            :param channel: (*str*) channel name or ``None`` or ``True`` for default ``shotbot``
            :param run_number: (*int*) run number for message title
            :param event_number: (*int*) event number for message title
        """
        
        try:
            if channel == None or channel == True:
                channel = "shotbot"
            if not hasattr(self,"slack_client"):
                self.slack_client = WebClient(token=self.settings["slack_token"])
            if fn==None:
                fn = "temp.png"
                plt.savefig(fn)
            title=""
            if run_number != None: title += "run " + str(run_number)
            if event_number != None: title += " | event " + str(event_number)
            response = self.slack_client.files_upload(channels=channel,
                                                      file=fn,
                                                      title=title
                                                     )
            if fn=="temp.png": os.remove(fn)
            return response
        except Exception as e:
            self.statusbar.showMessage(e)
            if self.debug: raise
                    
    def get_mean_image(self):
        """**Get a ``numpy array`` of the mean of all images over all events and runs**"""
        for i_, run in enumerate(self):
            if i_ == 0:
                img = run.get_mean_image()
            else:
                img += run.get_mean_image()
        return img / (i_+1)
    
    def get_mean_processed_image(self):
        """**Get a ``numpy array`` of the mean** of all processed images (:func:`~review.Event.get_processed_image`) over all events and runs"""
        for i_, run in enumerate(self):
            if i_ == 0:
                img, q_h,q_v = run.get_mean_processed_image()
            else:
                img_, q_h,q_v = run.get_mean_processed_image()
                img += img_
        return img / (i_+1), q_h,q_v

    def get_runs(self,*args):
        """**Returns a *list* of the runs contained in the object.**"""
        #self.update()
        if args == (): 
            return self.runs
        else:
            if len(args) == 1:
                run_numbers = args[0]
                if type(run_numbers) != list:
                    if run_numbers >= 0:
                        return [self.runs[run_numbers]]
                    else:
                        return [self.runs[self.get_run_numbers()[run_numbers]]]
            else:
                run_numbers = args
            selected_runs = [self.runs[x] for x in list(self.runs.keys()) if self.runs[x].run_number in run_numbers]
            return selected_runs

    def get_run(self,args):
        """
            **Get a specific run, or list of runs.**

            :param: *int*, *str* or *list* referencing the requested run(s) with their ``run_number`` or ``shot_type``
            
            - Returns::
                - a :class:`.Run` object, if called with *int* or only one run is selected
                - a *list*, if multiple runs are selected

            - Example::
                .. code-block:: python
                
                    import review as rv
                    experiment = rv.Experiment()

                    experiment.get_run(123)
                    experiment.get_run("mainshots")
                    experiment.get_run([123,124,125])
        """
        return self.get_runs(args)[0]

    def __len__(self):
        """**Returns an *int* containing the number of runs stored in the object.**"""
        return len(self.runs)

    def __getitem__(self, run):
        """
            **Select a single run, or a list of runs, or a slice**
            
            :param: *int*, *str*, *list*, or *slice*
            
            - Returns::
                - a :class:`.Run` object, if called with *int* or only one run is selected
                - a :class:`.Experiment` object, if multiple runs are selected. If you want a list of runs instead, have a look at :func:`~review.Experiment.get_event`.
                
                The advantage of obtaining the runs in a :class:`.Experiment` object is that you can use all the methods coming with it.

            - Example::
                .. code-block:: python
                
                    import review as rv
                    experiment = rv.Experiment()
                    experiment.update()
                    
                    experiment.get_run[123]          # select a single run
                    experiment.get_run[-1]           # last event
                    experiment.get_run["mainshots"]  # select mainshots only
                    experiment.get_run[123,124,125]  # select multiple runs
                    experiment.get_run["mainshots"].open_in_imageJ()  # open mainshots in imageJ
        """
        keys = list(self.runs.keys())
        if isinstance(run, str):
            run_type=run
            run = slice(np.min(keys),np.max(keys))
        else:
            run_type = None
        if isinstance(run, tuple):
            run = list(run)
        if isinstance(run, slice) or isinstance(run, list):
            if isinstance(run, slice):
                if run.start: 
                    start = run.start
                    if start < 0:
                        try:
                            start = keys[start]
                        except Exception as e:
                            start = np.min(keys)
                    if start == 0:
                        start = np.min(keys)
                else:
                    start = np.min(keys)
                if run.stop: 
                    try:
                        stop = run.stop[0]
                    except Exception as e:
                        stop = run.stop
                    if stop < 0:
                        stop = keys[stop]
                else:
                    stop = np.max(keys)
                runs = list(range(start, stop+1,run.step or 1))
            else:
                runs = run
            if len(runs) <= 1 and run_type == None:
                if runs == 0: runs = np.min(list(self.runs.keys()))
                return self.get_run(runs)
            else:
                # make a copy of self, without the runs dictionary------->
                return_runs = Experiment()
                attributes = [a for a in dir(self) if not a.startswith('__') and not callable(getattr(self, a))]
                for a in attributes:
                    if a != "runs" and a != "metadata":
                        setattr(return_runs,a,deepcopy(getattr(self, a)))
                return_runs.metadata=self.metadata
                return_runs.runs={}
                #<-------------
                #if hasattr(self,"main_window"):
                #    return_runs.main_window = self.main_window
                #if hasattr(self,"metadata"):
                #    return_runs.metadata = self.metadata
                #return_runs.settings = self.settings
                #return_runs.statusbar = self.statusbar
                for i_ , run in enumerate(runs):
                    try:
                        if self.is_new_run(run_number=run):
                            if run_type == None or ((run_type.find("preshot") > -1) and self.runs[run].preshots != {}) or ((run_type.find("mainshot") > -1) and self.runs[run].mainshots != {}) or ((run_type.find("postshot") > -1) and self.runs[run].postshots != {}):
                                # make a copy of each run---->

                                return_runs.runs[run] = Run(return_runs,run)
                                attributes = [a for a in dir(self.runs[run]) if not a.startswith('__') and not callable(getattr(self.runs[run], a))]
                                for a in attributes:
                                    if a != "runs" and a != "events" and a != "metadata"  and a != "preshots"  and a != "mainshots"  and a != "postshots":
                                        setattr(return_runs.runs[run],a,deepcopy(getattr(self.runs[run], a)))
                                return_runs.runs[run].metadata=self.metadata
                                return_runs.runs[run].preshots={}
                                return_runs.runs[run].mainshots={}
                                return_runs.runs[run].postshots={}
                                return_runs.runs[run].events={}
                                #<-------

                                if run_type != None:
                                    if run_type.find("preshot") > -1: 
                                        if self.runs[run].preshots != {}:
                                            del return_runs.runs[run]
                                        return_runs.runs[run].events = self.runs[run].preshots
                                        return_runs.runs[run].preshots = self.runs[run].preshots
                                    if run_type.find("mainshot") > -1: 
                                        if self.runs[run].mainshots != {}:
                                            del return_runs.runs[run]
                                        return_runs.runs[run].events = self.runs[run].mainshots
                                        return_runs.runs[run].mainshots = self.runs[run].mainshots
                                    if run_type.find("postshot") > -1: 
                                        if self.runs[run].postshots != {}:
                                            del return_runs.runs[run]
                                        return_runs.runs[run].events = self.runs[run].postshots
                                        return_runs.runs[run].postshots = self.runs[run].postshots
                                else:
                                    return_runs.runs[run].events = self.runs[run].events
                                    return_runs.runs[run].preshots = self.runs[run].preshots
                                    return_runs.runs[run].mainshots = self.runs[run].mainshots
                                    return_runs.runs[run].postshots = self.runs[run].postshots
                    except Exception as e:
                        try:
                            del return_runs.runs[run]
                        except Exception as e:
                            None 
                return return_runs
        if isinstance(run, int):
            return self.get_run(run)

        
    def __iter__(self):
        return ExperimentIterator(self)
        
    def append(self, run_number: int):
        """Adds a new run to the :class:`.Experiment` object."""
        self.runs[run_number] = Run(self,run_number)

    
    def is_new_run(self, filename=None, run_number=0, event_number = None):
        """
            **Checks whether a new event belongs to a new runs or a previous run**

            Supply either
            :param filename: *str* filename where the new event was read from, only if not read by API!
            or
            :param run_number: *int* of the run number
            :param event_number: *int*, optional. The event number. Can be omitted if the numbering is flat, i.e. only by run_numbers

            - Returns:: 
                - True, if ``event_number == 1``
                - True, if it is the first shot after the last ``postshot`` or other shot_type than ``preshot`` or ``mainshot``
                - True if no shot_type is given
                - False else

            - Example::
                .. code-block:: python
                
                    import review as rv
                    experiment = rv.Experiment()
                    experiment.get_run(123)
                    is_new = experiment.is_new_run(run_numner=123)        
        """
        if self.settings["fileSeparator"] == '' or self.settings["API"]:
            #flat numbering
            # the logic here would require a labbook
            try:
                if hasattr(self,"metadata"):
                    if run_number == self.metadata.get_run_for_event(run_number):
                        return True
                    else:
                        return False
                else:
                    return True
            except:
                return True
        else:
            if self.event_number_from_filename(filename) == 1:
                return True
                
        return False
    
    def update( self, for_run=None, cache = False , reload = False):
        """
            Looks for new runs and events, or only new events to a specific run.
            
            :param for_run: *int*, optional. If supplied, only new events to the run given by ``for_run = run_number`` are considered and added
            
            Returns::
                ``(is_new, new_events)``: 
                    - ``is_new`` is ``True`` if there are new events found
                    - ``new_events``: *dict* with key being the run_number, items are *list* of new event_numbers in the respective run
        """
        if not hasattr(self,"start_run"):
            if not hasattr(self,"settings"):
                self.start_run = 1
                self.end_run = 1e9
            else:
                self.start_run = self.settings["start_run"]
                self.end_run = self.settings["end_run"]
        self.statusbar.showMessage("Updating..." + str(self.start_run) + str(self.end_run))
        found_new=False
        new_events={}
        if hasattr(self,"metadata"): 
            self.statusbar.showMessage("Updating metadata...")
            self.metadata.update()
            for run in self.runs:
                self.runs[run].update(super_called=True)
                for event in self.runs[run].events:
                    self.runs[run].events[event].update(super_called=True)
                    
        #try:
        
        def add_new_run(found_new,new_events,filename_remote=None):
            try:
                #create new run series
                self.append(run_number)
                #create new event
                self.runs[run_number].append(event_number,filename_remote=filename_remote)#,filename_remote=filename)
                #self.runs[run_number][event_number].set_filename_remote(str(trainId))

                self.statusbar.showMessage("Found new run " + str(run_number))
                found_new = True
                self.last_run_read = run_number
                self.last_event_read = event_number
                new_events[run_number]=[event_number]
                if isinstance(cache,(list,np.ndarray)): 
                    for detector in cache: 
                        self.runs[run_number][event_number].get_image(detector=detector, cache_only = True, reload=reload)
                else:
                    if cache == True or reload: self.runs[run_number][event_number].get_image(cache_only = True, reload=reload)
                self.statusbar.showMessage("Added run+event"+str(found_new))
                return found_new,new_events
            except Exception as e:
                self.statusbar.showMessage(e)
                if self.debug: raise
        
        def add_new_event(found_new,new_events,filename_remote=None):
            try:
                if for_run == None or run_number==for_run:
                    #the run exists in the object, now check if the event already exists
                    #try:
                    main_for_run = self.main_for_run(run_number)
                    if not event_number in self.runs[main_for_run].events.keys():
                        #new event in current run
                        self.statusbar.showMessage("Found new event " + str(event_number))
                        found_new = True
                        self.last_run_read = main_for_run
                        self.last_event_read = event_number
                        #create new event
                        self.runs[main_for_run].append(event_number,filename_remote=filename_remote)#,filename_remote=filename)
                        try:
                            new_events[main_for_run].append(event_number)
                        except Exception as e:
                            new_events[main_for_run]=[event_number]
                        if isinstance(cache,(list,np.ndarray)): 
                            for detector in cache: self.runs[main_for_run][event_number].get_image(detector=detector, cache_only = True, reload=reload)
                        else:
                            if cache == True or reload: self.runs[main_for_run][event_number].get_image(cache_only = True, reload=reload)
                        self.statusbar.showMessage("Added event")
                    return found_new,new_events
            except Exception as e:
                self.statusbar.showMessage(e)
                if self.debug: raise
        
        self.statusbar.showMessage("Updating...")
        if self.settings["API"]:
            skipping = 0
            run_number= self.start_run*1
            while run_number <= self.end_run:
                if skipping == 10: continue
                try:
                    run = em.open_run(self.settings["proposal"], run_number,data='proc')  #Opening the given 
                    if run==0:
                        self.statusbar.showMessage("Skipping this run:" + str(run_number))
                        skipping += 1
                    else:
                        self.statusbar.showMessage("Run exists:" + str(run_number))
                        skipping = 0

                        event_number = run_number
                        if self.is_new_run(run_number=run_number,event_number=event_number) and not (run_number in self.runs.keys() or for_run != None):
                            self.statusbar.showMessage("Run is new:" + str(run_number))
                            #if found_new ==False and hasattr(self,"metadata"): self.metadata.update() 
                            found_new,new_events=add_new_run(found_new,new_events)
                        else:
                            found_new,new_events=add_new_event(found_new,new_events)
                except Exception as e:
                    self.statusbar.showMessage(e)
                    if self.debug: raise
                run_number += 1

        if self.settings["SFTP"]:
            # check if ssh connection is open
            if not hasattr(self, 'ssh'):
                # if not, open it
                try:
                    password = self.settings["password"]
                except Exception as e:
                    self.settings["password"]=None
                try:
                    username = self.settings["password"]
                except Exception as e:
                    self.settings["username"]=None
                self.ssh = SSH(self.settings["hostname"],self.settings["username"],password=self.settings["password"])
            # check for remote new files
            checkPattern = self.settings["remoteDataPath"]+self.settings["filePrefix"]+'*'+self.settings["fileMustHave"]+'*.'+self.settings["fileType"]
            self.statusbar.showMessage("Looking for new files on server...")
            files = self.ssh.get_file_list(checkPattern)
            if files != ['']:
                self.statusbar.showMessage("...found files.")
                for filename in files:
                    try:
                        #extract run number from file
                        run_number = self.run_number_from_filename(filename)
                        if run_number >= self.start_run and run_number <= self.end_run:
                            event_number = self.event_number_from_filename(filename)
                            #load new run if run is new
                            #if found_new ==False and hasattr(self,"metadata"): self.metadata.update() 
                            in1 = self.is_new_run(filename=filename,run_number=run_number,event_number=event_number) 
                            in2 = (run_number in self.runs.keys() or for_run != None)
                            if in1 and not in2:
                                found_new,new_events=add_new_run(found_new,new_events,filename_remote=filename)
                            else:
                                found_new,new_events=add_new_event(found_new,new_events,filename_remote=filename)
                    except Exception as e:
                        self.statusbar.showMessage(e)
                        if self.debug: raise
            else:
                self.statusbar.showMessage("...found no new files.")

        if self.settings["local"]:
            import glob
            #check for new files in localDataPath:
            checkPattern = self.settings["localDataPath"]+self.settings["filePrefix"]+'*'+self.settings["fileMustHave"]+'*.'+self.settings["fileType"]
            files = glob.glob(checkPattern)            
            for filename in files:
                filename=filename.replace("\\", "/")
                #extract run number from file
                run_number = self.run_number_from_filename(filename)
                if run_number >= self.start_run and run_number <= self.end_run:
                    try:
                        event_number = self.event_number_from_filename(filename)
                        #load new run if run is new
                        #if found_new ==False and hasattr(self,"metadata"): self.metadata.update() 
                        if self.is_new_run(filename=filename,run_number=run_number,event_number=event_number) and not (run_number in self.runs.keys() or for_run != None):
                            found_new,new_events=add_new_run(found_new,new_events)
                            self.runs[run_number][event_number].set_filename(filename)
                        else:
                            found_new,new_events=add_new_event(found_new,new_events)
                            main_for_run = self.main_for_run(run_number)
                            self.runs[main_for_run][event_number].set_filename(filename)
                    except Exception as e:
                        self.statusbar.showMessage(e)
                        if self.debug: raise

        if found_new:
            self.start_run = self.last_run_read
            try:
                self.latest_events = new_events
                self.main_window.new_last_event( self.runs[self.last_run_read].events[self.last_event_read] )
            except Exception as e:
                self.statusbar.showMessage(e)
                #if self.debug: raise
        #except Exception as e:
        #    return False
        return found_new
                      
    def run_number_from_filename(self,filename):
        """
            **Extract the run_number from a filename string**
            
            | The ``run_number`` is extracted from a path string based on the self.settings parameter. The filename path string is expected in the form of ``self.settings["filePrefix"] + str(run_number) + self.settings["fileEnd"]`` for flat numbering, or ``self.settings["filePrefix"] + str(run_number) + self.settings["fileSeparator"]`` for run+event labeling. 
            | To extract the event number, see :func:`~event_number_from_filename`
            
            :param: filename (*str*), string containing the path+filename of an event
            
            - Example::
                - './data/run_123.txt': Run number = 123 and each event is counting up, set ``self.settings["filePrefix"]='run_'`` and ``self.settings["fileEnd"] = '.txt'``
                - './data/123.tif': Run number = 123 and each event is counting up, set ``self.settings["filePrefix"]='/'`` and ``self.settings["fileEnd"] = '.tif'``
                - './data/run_123_1.txt':  Run number = 123 and event number = 1, set ``self.settings["filePrefix"]='run_'`` and ``self.settings["fileSeparator"] = '_'``
            
            - Returns::
                *int* of run_number
        """
        try:
            if self.settings["fileSeparator"] == '' or self.settings["API"]:
                #flat numbering
                return int(filename[find_all(filename,self.settings["filePrefix"])[-1]+len(self.settings["filePrefix"]):filename.find(self.settings["fileEnd"])])
            else:
                temp = filename[find_all(filename,self.settings["filePrefix"])[-1]+len(self.settings["filePrefix"]):]
                return int(temp[0:temp.find(self.settings["fileSeparator"])])
        except Exception as e:
            None

    def event_number_from_filename(self,filename):
        """
            **Extract the event_number from a filename string**
            
            | The ``event_number`` is extracted from a path string based on the self.settings parameter. The filename path string is expected in the form of 
            | ``self.settings["filePrefix"] + str(event_number) + self.settings["fileEnd"]`` for flat numbering (i.e. run number = event number!), or ``self.settings["filePrefix"] + str(run_number) + self.settings["fileSeparator"] + str(event_number) + self.settings["fileEnd"]`` for run+event labeling. 
            | To extract the run number, see :func:`~run_number_from_filename`
            
            :param: filename (*str*), string containing the path+filename of an event
            
            - Example::
                - './data/run_123.txt': Run number = event number = 123, set ``self.settings["filePrefix"]='run_'`` and ``self.settings["fileEnd"] = '.txt'``
                - './data/123.tif': Run number = event number = 123, set ``self.settings["filePrefix"]='/'`` and ``self.settings["fileEnd"] = '.tif'``
                - './data/run_123_1.txt':  Run number = 123 and event number = 1, set ``self.settings["filePrefix"]='run_'`` and ``self.settings["fileSeparator"] = '_'`` and ``self.settings["fileEnd"] = '.txt'``
            
            - Returns::
                *int* of event_number
        """
        try:
            if self.settings["fileSeparator"] == '' or self.settings["API"]:
                #flat numbering
                return self.run_number_from_filename(filename)
            else:
                temp = filename[find_all(filename,self.settings["filePrefix"])[-1]+len(self.settings["filePrefix"]):]
                return int(temp[temp.find(self.settings["fileSeparator"])+len(self.settings["fileSeparator"]):temp.find(self.settings["fileEnd"])])
        except Exception as e:
            return None

    def main_for_run(self,run_number):
        """
            **returns the run number of a given event**
            
            This method is expecially useful for flat indexing, as then run_numbers are treatet as event numbers and need to be sorted to the current real series (= run number).
            
            :param: run_number (*int*) (= event number in case of flat indexing)
            
            Returns::
                *int* of run_number
                
            Example::
                Imagine having 3 events in a series: a preshot (run/event number 10), mainshot (run/event number 11) and postshot (run/event number 12). Then ReView will treat this as a run with 3 events: run_number = 10, event_number = 10, 11, 12, respectively. This method will return ``10`` in this case.
                
        """
        if (self.settings["fileSeparator"] == '' or self.settings["API"]) and hasattr(self,"metadata"):
            #flat numbering
            return self.metadata.get_run_for_event(run_number)
        else:
            return run_number

    def connect_imageJ(self):
        """
            **Connect the imageJ functions of this class to an existing global imageJ instance, or create one if none is existing, yet.**
        """
        global ij_handler
        if ij_handler == None:
            scyjava.config.add_option('-Xmx6g')
            print("Image is being initialized. This can take a minute or two.")
            ij_handler = imagej.init(['sc.fiji:fiji:2.3.1', 'net.imagej:imagej-legacy'], headless=False)
        self.ij = ij_handler
        self.ij.ui().showUI()
            
    def open_in_imageJ(self,runs = None,detector = '', shot_type=None, processed=True,log=True, sort_by=None, limit=0):
        """
            **Open in imageJ**
            
            Opens all runs and events in imageJ and tries to group each run in a sparate stack.
            
            :param runs: (optional) runs of type :class:`Experiment`. If not specified, self is the default
            :param detector: (*optional*) *str* String of the requested detector. If not specified, the default edtector specified in :attr:`~settings` is used.
            :param shot_type: (optional) *str*, one of ``preshot``, ``mainshot``, ``postshot``
        """
        self.connect_imageJ()
        if runs == None: runs = self.runs
        if type(runs) != dict:
            runs = self.get_runs(runs)
            
        runs = Experiment(runs=runs)[-limit:]    
        
        if sort_by != None:
            # sort
            try:
                runs2plot = dict(sorted(runs.get_metadatas(sort_by).items(), key=lambda item: item[1]))
            except Exception as e:
                if self.debug: raise
                self.statusbar.showMessage(e)
        else:
            #unsorted
            runs2plot = runs

        for run in runs:
            run.open_in_imageJ(shot_type = shot_type,processed=processed,log=log)
            #self.runs[run].open_in_imageJ(shot_type = shot_type)
        
        if shot_type == None:
            #try to make a stack of all images open
            macro = "run(\"Images to Stack\", \"name=all_shots title=run_ use\");"
        else:
            #put only shot_type in stack
            macro = "run(\"Images to Stack\", \"name="+shot_type+" title="+shot_type+" use\");"
        try:
            self.ij.py.run_macro(macro)
        except Exception as e:
            None
            
    def get_run_numbers(self):
        """
            **Get a list of all run numbers in this object**
            
            Returns::
                (*list*) of run_numbers
        """
        return list(self.runs.keys())

    def get_metadatas(self,key: str,viz=False):
        """
            **Get a list of values of metadata with a certain key**
            
            For each run stored in this object, an entry is appended to the return list containing of the metadata for the key given as call parameter.
            
            :param: key (*str*) - The key (column) for which to search in the google spreadsheet labbook.
            :param: viz (*bool*) - ``True``, if the key is a vizualization metadata, or ``False`` if it is in the shotsheet
            
            Returns::
                (*list*) of metadata of variable type
        """
        l = {}
        for run in self:
            l[run.run_number] = run.get_metadata(key,viz=viz)
        return l

    def get_metadata_unit(self,key: str,viz=False):
        """
            **Returns the unit of the given metadata key**

            :param: key (*str*) - The key (column) for which to search in the google spreadsheet labbook.
            :param: viz (*bool*) - ``True``, if the key is a vizualization metadata, or ``False`` if it is in the shotsheet
            
            Returns::
                (*str*) of the unit of a metadata key
        """
        try:
            return self.metadata.get_unit(key,viz=viz)
        except Exception as e:
            None

class ExperimentIterator():
    def __init__(self, runs_object):
       # Team object reference
        self._runs_object = runs_object
        self._keys=list(runs_object.runs.keys())
       # member variable to keep track of current index
        self._index = 0
    def __next__(self):
        if self._index < (len(self._runs_object.runs)):
            result = self._runs_object.runs[self._keys[self._index]]
            self._index +=1
            return result
       # End of Iteration
        raise StopIteration

class Run():
    """
        **Run**
    """

    def __init__(self, runs, run_number):
        #new run is created
        self.debug = False
        self.runs = runs # reference to the parent
        try:
            self.main_window = self.runs.main_window
        except Exception as e:
            None
        self.settings = self.runs.settings
        self.statusbar = runs.statusbar
        self.run_number = run_number
        
        if hasattr(self.runs,"metadata"):
            self.metadata = self.runs.metadata
        
        self.events = {} #holds the events
        self.preshots={} # list of event_numbers that are preshots
        self.mainshots={}# list of event_numbers that are mainshots
        self.postshots={}# list of event_numbers that are postshots

        #self.runs.statusbar.showMessage("New run "+str(run_number)+"!")

        try:
            self.main_window.new_run(self)
        except Exception as e:
            None
                        
    def get_mean_image(self, detector = ''):
        for i_, event in enumerate(self):
            if i_ == 0: 
                img = event.get_image(detector=detector)
            else:
                img += event.get_image(detector=detector)
        return img/(i_+1)

    def get_mean_processed_image(self):
        for i_, event in enumerate(self):
            if i_ == 0:
                img, q_h,q_v = event.get_processed_image()
            else:
                img_, q_h,q_v = event.get_processed_image()
                img += img_
        return img / (i_+1), q_h,q_v

#    def get_preshots(self):
#        return self.get_events("preshots")

#    def get_preshot(self):
#        return self.get_event("preshots")
            
#    def get_mainshots(self):
#        return self.get_events("mainshots")

#    def get_mainshot(self):
#        return self.get_event("mainshots")
            
#    def get_postshots(self):
#        return self.get_events("postshots")

#    def get_postshot(self):
#        return self.get_event("postshot")
            
    def update(self,super_called=False):
        if not super_called and hasattr(self.runs,'metadata'):
            self.metadata.update()
        #self.runs.update(for_run=self.run_number)
        for event in self.events:
            self.events[event].update(super_called=super_called)
    
    def get_events(self,*args,**kwargs):
        #self.runs.update()
        if args == (): 
            return self.events
        else:
            event_numbers = None
            shot_types = None
            if isinstance(args[0], (int, np.integer)):
                event_numbers = args[0]
            if type(args[0]) == str:
                shot_types=args[0]
        if "event_numbers" in kwargs.keys() or  "event_number" in kwargs.keys(): event_numbers = kwargs["event_numbers"]
        if "shot_types" in kwargs.keys() or "shot_types" in kwargs.keys(): shot_types = kwargs["shot_types"]
            
        if isinstance(event_numbers, (int, np.integer)):
            if event_numbers >= 0:
                event_numbers = [event_numbers]
            else:
                event_numbers = [self.get_event_numbers()[event_numbers]]
        if type(shot_types) == str:
            shot_types = [shot_types]
        try:
            if event_numbers == None:
                selected_events = [self.events[x] for x in list(self.events.keys()) if (self.events[x].shot_type in shot_types or self.events[x].shot_type+"s" in shot_types)]
            else: 
                if shot_types == None:
                    selected_events = [self.events[x] for x in list(self.events.keys()) if self.events[x].event_number in event_numbers]
                else:
                    selected_events = [self.events[x] for x in list(self.events.keys()) if self.events[x].event_number in event_numbers and (self.events[x].shot_type in shot_types or self.events[x].shot_type+"s" in shot_types)]
        except Exception as e:
            selected_events = []
        return selected_events

    def get_event(self,*args,**kwargs):
        try:
            if args == () and kwargs=={} and len(self.events)==1:
                return self.events[list(self.events.keys())[0]]
            return self.get_events(*args,**kwargs)[0]
        except Exception as e:
            None
    
    def __len__(self):
        return len(self.events)

    def __getitem__(self, event):
        if isinstance(event, int):
            if event == 0: event = np.min(list(self.events.keys()))
            return self.get_event(event)
        if isinstance(event,str):
            try:
                if event[-1] != "s":
                    # singular: return the event
                    events = self.get_event(event).event_number
                else:
                    #plural: return the run with the selected events
                    events =[]
                    for event_ in self.get_events(event):
                        events.append(event_.event_number)
            except Exception as e:
                events=[]
        else:
            if isinstance(event, slice):
                keys = list(self.events.keys())
                if event.start: 
                    start = event.start
                    if start < 0:
                        try:
                            start = keys[start]
                        except Exception as e:
                            start = np.min(keys)
                    if start == 0:
                        start = np.min(keys)
                else:
                    start = np.min(keys)
                if event.stop: 
                    try:
                        stop = event.stop[0]
                    except Exception as e:
                        stop = event.stop
                    if stop < 0:
                        stop = keys[stop]
                else:
                    stop = np.max(keys)
                events = list(range(start, stop+1,event.step or 1))
            else:
                events=event
        if isinstance(events,int):
            return self.get_event(events)
        if events==[]:
            return None
        else:

            # make a copy of each run---->
            return_events = Run(self,self.run_number)
            attributes = [a for a in dir(self) if not a.startswith('__') and not callable(getattr(self, a))]
            for a in attributes:
                if a != "runs" and a != "events" and a != "metadata"  and a != "preshots"  and a != "mainshots"  and a != "postshots":
                    setattr(return_events,a,deepcopy(getattr(self, a)))
            return_events.metadata=self.metadata
            return_events.preshots={}
            return_events.mainshots={}
            return_events.postshots={}
            return_events.events={}

            for event in events:
                try:
                    return_events.events[event] = self.events[event]

                    #return_events.append(return_events.get_events(event).event_number)
                    #return_events[-1].set_filename(return_events.events[event].filename)
                    #return_events[-1].set_filename_remote(return_events.events[event].filename_remote)
                except Exception as e:
                    None
            return return_events
 
    def __iter__(self):
        return RunIterator(self)
    
    def append(self,event_number,filename=None,filename_remote=None):
        self.events[event_number] = Event(self, event_number,filename=filename,filename_remote=filename_remote)
        
    def open_in_imageJ(self,events=None,setector = '', shot_type=None,processed=True,log=True):
        try:
            self.runs.connect_imageJ()
            if events == None: events = self.events
            if type(events) != dict:
                events = self.get_events(events)
            for event in events:
                if shot_type == None or self.events[event].shot_type == shot_type:
                    image = self.events[event].open_in_imageJ(detector=detector,processed=processed,log=log)
        except Exception as e:
            self.statusbar.showMessage(e)
            if self.debug: raise
            None
                
        if len(self.events) > 1:
            macro = "run(\"Images to Stack\", \"name=run_"+str(self.run_number)+" title=run_"+str(self.run_number)+"-event use\");"
            try:
                self.runs.ij.py.run_macro(macro)
            except Exception as e:
                None
                
    def get_metadata(self,key,viz=False):
        try:
            if self.settings["fileSeparator"] == '' or self.settings["API"]:
                    #flat numbering
                try:
                    values=[]
                    for event_number in list(self.mainshots.keys()):
                        values.append(self.mainshots[event_number].get_metadata(key,viz=viz))
                    #if len(values)==1: values=values[0]
                    values=values[0]
                    return values
                except Exception as e:
                    # or of first event
                    return self.events[0].get_metadata(key,viz=viz)                    
            else:
                return self.metadata.get(key,self.run.run_number, event_number = self.event_number)  
        except Exception as e:
            None

    def get_metadata_unit(self,key,viz=False):
        try:
            return self.metadata.get_unit(key,viz=viz)
        except Exception as e:
            None

    def get_event_numbers(self):
        return list(self.events.keys())

    def get_metadatas(self,key,viz=False):
        l = {}
        for event in self:
            l[event.event_number] = event.get_metadata(key,viz=viz)
        return l
    
    def plot_overview(self,detectors=[''],last_event_only=False,linewidth=16,smooth=2,bg_subtraction=True,slack=False,norm="max", savefig=False,processed=True,recalc=False):
        figs = self.plot_detectors(detectors=detectors,last_event_only=last_event_only,linewidth=linewidth,smooth=smooth,norm="", savefig=savefig,processed=processed,recalc=recalc)
            
        # horizontal profiles------------------------------------------------------
        for direction in ["horizontal","vertical"]:
            figs.append(
                self.plot_profiles(detectors = detectors,direction=direction,linewidth=linewidth,smooth=smooth,bg_subtraction=bg_subtraction,slack=slack,norm=norm, savefig=savefig,recalc=recalc)
            )
        
        fn = self.settings["filePrefix"] + str(self.run_number) + self.settings["fileEnd"] + "overview"
        self.runs.savefigs("runs",fn,figs,slack=slack,run_number=self.run_number)        
        
    def plot_detectors(self,detectors=[''],last_event_only=True,linewidth=16,smooth=2,slack=False,norm="max", savefig=False,processed=True,recalc=False):
        if last_event_only: 
            events = [self[int(np.max(self.get_event_numbers()))]]
        else:
            events = self
            
        figs=[]
        for ev in events:
            fn = ev.plot_detectors(detectors=detectors,linewidth=linewidth,smooth=smooth,slack=slack,norm=norm, savefig=savefig,processed=processed,recalc=recalc)
            figs.append(fn)
            
        return figs

    def plot_profiles(self,detectors=[''],direction="horizontal", linewidth=16,smooth=2,bg_subtraction=True,slack=False,norm="max", savefig=False, processed=True,recalc=False):
        """
            **Plot the horizontal profiles of all events in one graph**
            
            This is a plotting method using ``matplotlib``. It plots the detector 2d image horizontal profile of the standard diognostics, or of all the detectors given by parameter `detectors`` next to each other, labeling with metadata using the class :class:`.Metadata`. 
            It is quite specific for a RELAX experiment at the moment. We assume **JF4 is the SAXS detector**, used for background correction by default, and adding the ``1/q^2`` reference line. 
            
            :param bg_subtraction: *bool*, default: ``True``. Enable background subtraction for the SAXS detector. 
            :param detectors: *str* Detectors that shall be included in the overview. By default only the default detector specified in the settings is displayed.
            :param last_event_only: *bool*, default: ``True``. Plots only the camera image of the last event (lineouts for all runs and events)
            :param linewidth: *int* Thickness of the line of the profile over which to average.
            :param smooth: *float* Gaussian_filter width
            :param sort_by: *str* metadata key used for sorting the lineouts by color

            Example:: 
                This function can be easily used to plot only a subset (sorted by delay by default), also. E.g. by filtering for mainshots only, that have a delay between 20 ps: 

                .. code-block:: python

                   filtered = Experiment(runs={k:v for k,v in experiment.runs["mainshots"].items() if v.get_metadata("delay") != None and v.get_metadata("delay") > 20}) # filter for certain criteria
                   filtered.plot_profiles() 
                   
                See also :func:`~plot_detectors`, :func:`~plot_overview`, :meth:`Run.plot_overview`, :meth:`Run.plot_detectors`, :meth:`Run.plot_profiles`, :meth:`Event.plot_overview`, :meth:`Event.plot_detectors`, :meth:`Event.plot_profile`, :meth:`Event.plot_profiles`. More information can also be found in the :ref:`plotting section<Plotting>`. 
                   
            .. note::
            
                This plotting method uses the post-processed image data optained from  
                :func:`~review.Event.get_processed_image`. This uses metadata in the google labbook 
                to rotate, correct perspective, darkfielding, flatfielding etc.

            .. note:: 
                This plotting method is very specific w.r.t. background subtraction. 
                This needs to be abstracted!
                                  
        """
        colors=["green","blue","red","black"]
        fig, ax = plt.subplots(nrows=1, ncols=len(detectors),figsize=(11*len(detectors),6))        
        if not (isinstance(ax,list) or isinstance(ax,np.ndarray)): 
            ax=[ax]

        for j_, detector in enumerate(detectors):
            if detector == '': detector = self.settings["detector"]
            if isinstance(norm,str): 
                norm_ = norm
            else:
                norm_ = norm[j_]

            min_=1e99
            max_=0
            max__=0

            if isinstance(smooth,list) or isinstance(smooth,np.ndarray): 
                smooth_ = smooth[j_]
            else:
                smooth_ = smooth
            if isinstance(linewidth,list) or isinstance(linewidth,np.ndarray): 
                linewidth_ = linewidth[j_]
            else:
                linewidth_ = linewidth

            for i_,ev in enumerate(self):
                
                if i_==0 and detector == "JF4": 
                    add_q2=True
                else:
                    add_q2=False
                if detector != "JF4": bg_subtraction = False
                
                min_,max_=ev.plot_profile(detector,direction=direction,
                                                     ax=ax[j_],
                                                     min_=min_,
                                                     max_=max_,
                                                     color=colors[i_],
                                                     add_q2=add_q2,
                                                     linewidth=linewidth_,
                                                     smooth=smooth_,
                                                     bg_subtraction=bg_subtraction,
                                                     norm=norm_,
                                                     processed=processed,
                                                     recalc=recalc,
                                                     savefig = savefig
                                                    )
                
            if detector == "JF4": 
                min_ = 0.00125 * max_
                max_ = 1.6*max_
            ax[j_].set_ylim([0.75*min_,max_*1.25])
            #ax[j_].set_xlim([np.min(np.where(lh>0)),np.max(np.where(lh>0))])
            if j_ == 0: ax[j_].legend()
            ax[j_].set_title(detector+' | bg sub: ' + str(bg_subtraction))
        fig.suptitle(direction+" profiles run "+str(self.run_number)+" | delay "+str(self.get_metadata("delay"))+" "+str(self.get_metadata_unit("delay")))
        plt.tight_layout()

        fn = self.settings["filePrefix"] + str(self.run_number) + self.settings["fileEnd"] + direction+"Profiles"
        if savefig:
            self.runs.savefig("runs",fn)

        if slack != False:
            self.runs.send_fig_slack(slack, run_number = self.run_number)

        plt.show()
        
        return fn
                
class RunIterator():
    def __init__(self, run_object):
       # Team object reference
        self._run_object = run_object
        self._keys=list(run_object.events.keys())
       # member variable to keep track of current index
        self._index = 0
    def __next__(self):
        if self._index < (len(self._run_object.events)):
            result = self._run_object.events[self._keys[self._index]]
            self._index +=1
            return result
       # End of Iteration
        raise StopIteration
        


class Event():
    """
        **Class to hold all data relevant for a single event (=shot)**
    """
    
    def __init__(self, run, event_number, filename=None, filename_remote=None):
        """
            **Init function for an event object**
            Here goes some text!
        """

        self.debug = False
        
        #new run is created
        self.runs = run.runs
        self.run = run
        if hasattr(self.runs, "metadata"): self.metadata = self.runs.metadata
        self.settings = self.runs.settings
        self.statusbar = self.runs.statusbar
        try:
            self.main_window = self.runs.main_window
        except Exception as e:
            None
            
        self.event_number = event_number
        self.set_filename(self.filename_from_run_number())
        self.set_filename_remote(filename_remote)

        if hasattr(self.runs,"metadata"):
            self.metadata = self.runs.metadata
        
        self.update(super_called=True) # reads the data 
        
        #self.runs.statusbar.showMessage("New event "+str(event_number)+" for run "+str(self.run.run_number)+"!")
        try:
            self.main_window.new_event(self)
        except Exception as e:
            None
            
        # if cache wanted, get the data now in order to cache!
        #if (self.runs.settings["cache"] and not self.runs.settings["local"]) or 
        if (self.runs.settings["cacheMemory"] == -1):
            self.get_image(cache_only = True)
            self.get_processed_image(cache_only = True)

    def plot_overview(self,detectors=[''],linewidth=16,smooth=2,bg_subtraction=True,slack=False,norm="max",savefig=False, processed=True,recalc=False):
        fig = self.plot_detectors(detectors=detectors,linewidth=linewidth,smooth=smooth,norm="",savefig=savefig,slack=slack,processed=processed,recalc=recalc)
            
        # horizontal profiles------------------------------------------------------
        for direction in ["horizontal","vertical"]:
            self.plot_profiles(detectors = detectors,direction=direction,linewidth=linewidth,smooth=smooth,bg_subtraction=bg_subtraction,slack=slack,norm=norm,savefig=savefig,processed=processed,recalc=recalc)
    
        return fig

    def plot_detectors(self,detectors=[''],linewidth=16,smooth=2,slack=False,norm="max",savefig=False,processed=True,recalc=False):
        fig, ax = plt.subplots(nrows=1, ncols=len(detectors),figsize=(11*len(detectors),10))
        if len(detectors) == 1: ax=[ax]
        for j_, detector in enumerate(detectors):
            if isinstance(norm,str): 
                norm_ = norm
            else:
                norm_ = norm[j_]
            try:
                if detector == '': detector = self.settings["detector"]
                if isinstance(smooth,list) or isinstance(smooth,np.ndarray): 
                    smooth_ = smooth[j_]
                else:
                    smooth_ = smooth
                if isinstance(linewidth,list) or isinstance(linewidth,np.ndarray): 
                    linewidth_ = linewidth[j_]
                else:
                    linewidth_ = linewidth

                if processed:
                    img, q_h,q_v,minimum,maximum = self.get_processed_image(detector=detector, smooth=smooth_,set_metadata=True,linewidth=linewidth_,norm="",minmax=True,recalc=recalc)
                else:
                    img, q_h,q_v,minimum,maximum = self.get_image(detector=detector)
                    
                if detector == "Zyla": 
                    log = False
                    unitxy = ""
                else:
                    log = True
                    unitxy = "$[nm^{-1}]$"
                    
                extent=[np.min(q_v),np.max(q_v),np.min(q_h),np.max(q_h)]
                    
                if not log:
                    im_ = ax[j_].imshow(img,cmap=rofl.cmap(),extent=extent)
                    t=""
                else:
                    max_=np.max(img)
                    im_ = ax[j_].imshow(np.log10(img),vmin=np.log10(max_*1e-4),vmax=np.log10(max_),cmap=rofl.cmap(),extent=extent)
                    t="log "
                try:
                    unit = self.settings["unit_"+detector]
                except:
                    unit="ADU"
                ax[j_].set_xlabel(r'Horizontal q '+unitxy)
                ax[j_].set_ylabel(r'Vertical q '+unitxy)
                ax[j_].set_title(t + detector+" | min: "+str(np.round(minimum*self.I0,2))+" "+unit+" | max: "+str(np.round(maximum*self.I0,2))+unit)
                if j_ == 0: ax[j_].set_ylabel("q_y")
                
#                divider = make_axes_locatable(ax[j_])
                plt.style.use("dark_background")
                cax = ax[j_].inset_axes([0.055,0.9,.37, .045]) 
                #cax = divider.append_axes('right', size='5%', pad=0.0)
                cb = fig.colorbar(im_, cax=cax, orientation='horizontal')
                # set colorbar tick color
                cb.ax.xaxis.set_tick_params(color="white")
                if norm in ["","photons","ADU"]: 
                    if detector == "JF4":
                        norm = "photons"
                    else:
                        norm = "ADU"
                cb.set_label('norm = ' + norm + ", log: " + str(log))

                # set colorbar edgecolor 
                cb.outline.set_edgecolor("white")

                # set colorbar ticklabels
                plt.setp(plt.getp(cb.ax.axes, 'xticklabels'), color="white")                
                plt.style.use("default")
                fig.tight_layout()
            except Exception as e:
                self.statusbar.showMessage(e)
                None                
        t = "Run "+str(self.run.run_number)+" | Event "+str(self.event_number)+" | " + self.shot_type + " | I0 = " + str(self.I0/1e10)+"*1e10"
        if self.shot_type == "mainshot": t+=" | delay "+str(self.get_metadata("delay"))+" "+str(self.get_metadata_unit("delay"))
        fig.suptitle(t)
        plt.tight_layout()
        
        fn = self.settings["filePrefix"] + str(self.run.run_number) + self.settings["fileSeparator"] + str(self.event_number) + self.settings["fileEnd"] + "detectors2D"
        if savefig:
            self.runs.savefig("events",fn)

        if slack != False:
            self.runs.send_fig_slack(slack, run_number = self.run.run_number, event_number = self.event_number)
            
        plt.show()
        return fn

    def plot_profiles(self,detectors=[''],direction="horizontal",color="blue",linewidth=16,smooth=2,bg_subtraction=False,slack=False,norm="max",savefig=False,processed=True,recalc=False):
        fig, ax = plt.subplots(nrows=1, ncols=len(detectors),figsize=(11*len(detectors),6))
        if not (isinstance(ax,list) or isinstance(ax,np.ndarray)): ax=[ax]

        for j_, detector in enumerate(detectors):
            if detector == '': detector = self.settings["detector"]
            
            if isinstance(norm,str): 
                norm_ = norm
            else:
                norm_ = norm[j_]

            if detector == "JF4": 
                add_q2=True
            else:
                bg_subtraction = False
                add_q2=False

            min_=1e99
            max_=0
            max__=0
            if isinstance(smooth,list) or isinstance(smooth,np.ndarray): 
                smooth_ = smooth[j_]
            else:
                smooth_ = smooth

            if isinstance(linewidth,list) or isinstance(linewidth,np.ndarray): 
                linewidth_ = linewidth[j_]
            else:
                linewidth_ = linewidth

            min_,max_=self.plot_profile(detector,direction=direction,
                                        ax=ax[j_],
                                        min_=min_,
                                        max_=max_,
                                        color=color,
                                        add_q2=add_q2,
                                        linewidth=linewidth_,
                                        smooth=smooth_,
                                        bg_subtraction=bg_subtraction,
                                        norm=norm_,
                                        processed=processed,
                                        recalc=recalc
                                       )
                
            if detector == "JF4": 
                min_ = 0.00125 * max_
                max_ = 1.6*max_
            ax[j_].set_ylim([0.75*min_,max_*1.25])
            #ax[j_].set_xlim([0,:])#np.min(np.where(lh>0)),np.max(np.where(lh>0))])
            if j_ == 0: ax[j_].legend()
            ax[j_].set_title(detector+' | bg sub: ' + str(bg_subtraction))
        t= direction+" profiles run "+str(self.run.run_number)+" | "+"Event "+str(self.event_number)+" | "+self.shot_type
        if self.shot_type=="mainshot": t+= " | delay "+str(self.get_metadata("delay"))+" "+str(self.get_metadata_unit("delay"))
        fig.suptitle(t)
        plt.tight_layout()

        fn = self.settings["filePrefix"] + str(self.run.run_number) + self.settings["fileSeparator"] + str(self.event_number) + self.settings["fileEnd"] + direction+"Profiles"
        if savefig:
            self.runs.savefig("events",fn)

        if slack != False:
            self.runs.send_fig_slack(slack, run_number = self.run.run_number, event_number = self.event_number)

        plt.show()
            
        return fn
        
    def plot_profile(self,detector='',direction="horizontal",ax=None,max_=0,min_=1e99,color="red",add_q2 = False,linewidth=16,smooth=2,bg_subtraction=False,slack=False,draw_background=None,norm = "max",savefig=False,sigma=0.4,processed=True,recalc=False):
        """
            **Plot a horizontal profile**, either in a separate figure, or, if ``ax`` is given, plot it on an existing axis.
            
            :param detector: (*str*) detector which to plot, or ``''`` to use standard detector defined in :ref:`settings <settings>`. 
            :param ax: (*axis*) plot target
            :param min_: (*float*) minimum of prelivious plots in axis, can be used to compare the min of the current plot with that of previous and return a global min
            :param max_: (*float*) maximum of prelivious plots in axis, can be used to compare the max of the current plot with that of previous and return a global max
            :param color: color of the plot line
            :param add_q2: (*bool*) add the q^2 fits for comparions
            :param linewidth: (*int*) thickness of the region to sim the profile
            :param norm: (*str*) normalization, one of ``max``, ``I0``, ``raw``
        """
        if detector == '': detector = self.settings["detector"]
        if ax == None:
            fig, ax = plt.subplots(nrows=1, ncols=1,figsize=(11,6))
            standalone=True
        else:
            standalone=False
        #if not isinstance(ax,list): ax=[ax]
            
        # === 1. Background subtraction ===
        #if detector == "JF4":
        #    bg_subtraction = True
        if bg_subtraction:
            if draw_background == None:draw_background = False
        else:
            if draw_background == None:draw_background = False
        if detector == "Zyla":
            bg_subtraction=False
            draw_background = False

        self.statusbar.showMessage("Getting line profile:")
        lh,lv,lhb,lvb,q,qy = self.get_profiles(bg_correction=bg_subtraction,detector=detector,linewidth = linewidth,smooth=smooth, norm = norm,processed=processed,recalc=recalc)
        if direction == "vertical":
            lh = lv
            lhb = lvb
            q=qy

        # min_, max_ reuturns the min max of the plot
        if np.max(lh) > max_: max_=np.max(lh)
        if detector != "JF4":
            min_=0
        else:
            if np.min(lhb[int(len(lhb)/2)-20:int(len(lhb)/2)+20]) < min_: min_= np.min(lhb[int(len(lhb)/2)-20:int(len(lhb)/2)+20])
        if self.shot_type == "mainshot":
            t = " | delay: " + str(self.get_metadata("delay"))+self.get_metadata_unit("delay")
        else:
            t = ''
        t = "Event "+str(self.event_number)+": "+self.shot_type + t
        # normalization output
        self.statusbar.showMessage("Getting line profile again, normalized to I0, in order to be able to set labels:")
        lh_,lv_,lhb_,lvb_,q_,qy_ = self.get_profiles(bg_correction=bg_subtraction,detector=detector,linewidth = linewidth,smooth=smooth, norm = "I0",processed=processed,recalc=False)
        if direction == "vertical":
            lh_ = lv_
            lhb_ = lvb_
        maximum = np.max(lh_)
        t += " | I/I0 = "+str(np.round(maximum*1e10,2))+" arb.u."
        
        if add_q2:
            #plot 1/q^2
            x1 = np.abs(np.argmax(lh))
            q_ = np.abs((q+1e-9)/q[x1])
            l=lh*0 + np.max(lh) * 1/q_**2
            ax.semilogy(q,l,color="black")    
            l=lh*0 + np.max(lh) * 1/q_**2 * np.exp(-(q_**2-1)*sigma**2)
            ax.semilogy(q,l,c='0.3')    
            l=lh*0 + np.max(lh) * 1/q_**2 * np.exp(-(q_**2-1)*4*sigma**2)
            ax.semilogy(q,l,c='0.525')    
            l=lh*0 + np.max(lh) * 1/q_**2 * np.exp(-(q_**2-1)*9*sigma**2)
            ax.semilogy(q,l,c='0.80')    
            
            l=lh*0 + np.max(lh) * 1/q_**3
            ax.semilogy(q,l,color="black",linestyle="--")    
            l=lh*0 + np.max(lh) * 1/q_**3 * np.exp(-(q_**2-1)*sigma**2)
            ax.semilogy(q,l,c='0.3',linestyle="--")    
            l=lh*0 + np.max(lh) * 1/q_**3 * np.exp(-(q_**2-1)*4*sigma**2)
            ax.semilogy(q,l,c='0.525',linestyle="--")    
            l=lh*0 + np.max(lh) * 1/q_**3 * np.exp(-(q_**2-1)*9*sigma**2)
            ax.semilogy(q,l,c='0.80',linestyle="--")    
            #ax.set_xlim([0,len(lh)])

        #confidence intervals
        if bg_subtraction:
            sigma =np.sqrt((lh_ + lhb_) + lhb_)*np.sqrt(self.I0)
        else:
            sigma = np.sqrt(lh_)*np.sqrt(self.I0)
        if norm == "I0":
            sigma = sigma / self.I0
        else:
            if norm == "max":
                sigma = sigma/self.I0/np.max(lh_)
        
        if detector == "Zyla":
            log = False
        else:
            log = True
        if not log:
            ax.plot(q,lh,color=color, label=t)
        else:
            ax.semilogy(q,lh,color=color, label=t)
        ax.fill_between(q,gaussian_filter(lh-sigma,5),gaussian_filter(lh+sigma,5),color=color, alpha=0.1)
        if draw_background: ax.semilogy(q,lhb,color=color,linestyle="--")

        if norm == "I0":
            ax.set_ylabel("I/I0 (arb. u.)")
        else:
            if norm == "max":
                ax.set_ylabel("photons (normalized)")
            else:
                ax.set_ylabel("photons")
        ax.set_xlabel("q (arb. u.)")
        
        ax.set_xlim([np.min(q),np.max(q)])#[np.min(np.where(lh>0)),np.max(np.where(lh>0))])
        if standalone:
            if detector == "JF4": 
                min_ = 0.00125 * max_
                max_ = 1.6*max_
            ax.set_ylim([0.75*min_,max_*1.25])
            t = direction+" profile run "+str(self.run.run_number)+" | Event "+str(self.event_number)+" | " + self.shot_type
            if self.shot_type == "mainshot": t+=" | delay "+str(self.get_metadata("delay"))+" "+str(self.get_metadata_unit("delay"))
            ax.set_title(detector+" | " + t+' | bg sub: ' + str(bg_subtraction))
            plt.tight_layout()

            if savefig:
                fn = self.settings["filePrefix"] + str(self.run.run_number) + self.settings["fileSeparator"] + str(self.event_number) + self.settings["fileEnd"] + direction+"Profile"+detector
                self.runs.savefig("events",fn)

            if slack != False:
                self.runs.send_fig_slack(slack, run_number = self.run.run_number, event_number = self.event_number)
            plt.show()

        return min_,max_

    
    def set_filename(self,filename):
        self.filename = filename

    def set_filename_remote(self,filename_remote):
        self.filename_remote = filename_remote
        if self.settings["cache"]:
            directory = self.settings["localDataPath"]
        else: 
            directory = "." # dir for temporary data download
        # filename for cache
        if self.settings["SFTP"]:
            #same filename as remote file
            self.filename = directory+'/'+filename_remote[find_all(filename_remote,"/")[-1]+1:] #os.sep
        if self.settings["API"]:
            self.filename = self.filename_from_run_number()
        
    def filename_from_run_number(self):
        if self.settings["cache"]:
            directory = self.settings["localDataPath"]
        else: 
            directory = "."
        if self.settings["fileSeparator"] == '':
            #flat numbering
            return directory+'/'+self.settings["filePrefix"]+str(self.event_number)+self.settings["fileEnd"]+self.settings["fileMustHave"]+'.'+self.settings["fileType"]
        else:
            return directory + '/'+self.settings["filePrefix"]+str(self.run.run_number)+self.settings["fileSeparator"]+str(self.event_number)+self.settings["fileEnd"]+self.settings["fileMustHave"]+'.'+self.settings["fileType"]

    def is_shot_type(self,shot_type):
        if hasattr(self.runs,"metadata"):
            return self.get_metadata("shot_type") == shot_type
        else:
            if self.filename.find(shot_type) > -1: 
                return True
            else:
                if shot_type == "preshot" and self.event_number < 3: return True
                if shot_type == "mainshot" and self.event_number == 3: return True
                if shot_type == "postshot" and self.event_number > 3: return True
        return False

    def update(self,super_called=False):
        if hasattr(self.runs,'metadata'): 
            if not super_called:
                self.metadata.update()
            #self.runs.metadata.update()
            self.run.preshots.pop(self.event_number,None)
            self.run.mainshots.pop(self.event_number,None)
            self.run.postshots.pop(self.event_number,None)
            if self.is_shot_type("preshot"): 
                self.run.preshots[self.event_number]=self
                self.shot_type = "preshot"
            else:
                if self.is_shot_type("mainshot"): 
                    self.run.mainshots[self.event_number]=self
                    self.shot_type = "mainshot"
                else:
                    if self.is_shot_type("postshot"): 
                        self.run.postshots[self.event_number]=self
                        self.shot_type = "postshot"
                    else:
                        self.shot_type = self.get_metadata("shot_type")
        
    def get_image(self, cache_only = False,detector='',reload=False):
        self.statusbar.showMessage("Getting new event image: " + str(self.event_number) + '...' + detector)
        if not hasattr(self,"_image_"+detector) or reload:
            if (self.runs.settings["local"] or (self.runs.settings["cache"] and not reload)):# and not cache_only:
                #read file from local cache
                image = self.get_image_from_local_file(detector=detector)
            else: 
                image=None
                #setattr(self,"_image_"+detector,None)
            if not isinstance(image,np.ndarray) or reload:
                if self.runs.settings["SFTP"]:
                    image = self.get_image_from_SFTP(detector=detector)
                else:
                    if self.runs.settings["API"]:
                        image = self.get_image_from_API(detector=detector)
            
            # take care of local cache:
            if self.settings["cacheMemory"] > -1 and isinstance(image,np.ndarray):
                setattr(self,"_image_" + detector, image)
                # remove old cache
                if len(self.runs.cached) >= self.settings["cacheMemory"] and self.settings["cacheMemory"] > 0:
                    for ev in self.runs.runs[self.runs.cached.pop(0)]:
                        images = [a for a in dir(ev) if (a.startswith('_image') or a.startswith('_processed_image'))]
                        for a in images:
                            try:
                                delattr( ev , a )
                            except Exception as e:
                                None
                # add this image to the list of cached images
                if not self.run.run_number in self.runs.cached and not self.is_shot_type("darkfield") and not self.is_shot_type("flatfield"):
                    self.runs.cached.append(self.run.run_number)
            self.statusbar.showMessage("Got new event image: " + str(self.event_number) + '...' + detector)
            if not cache_only:
                return image
        else:
            self.statusbar.showMessage("Got new event image from local memory: " + str(self.event_number) + '...' + detector)
            if not cache_only: return getattr(self,"_image_"+detector)
        self.statusbar.showMessage("Cached new event image: " + str(self.event_number) + '...' + detector)
                    
        #... flatfield, darkfield, calibrate, distortion, rotation, subtract background

    def get_profiles(self,processed = True, bg_correction=True, detector='',recalc = False, linewidth=10,smooth = None, center=None, marker=None, deg=None, set_metadata=False, size=None, norm="max"):
        if detector == '': detector = self.settings["detector"] # to the the respective markers below
        self.statusbar.showMessage("Getting lineprofile for detector " + detector)
        if processed: 
            self.statusbar.showMessage("  Getting processed imag...e")
            image, q_h,q_v = self.get_processed_image(detector=detector,recalc=recalc,show_markers=False,smooth=smooth, center=center, marker=marker, deg=deg, set_metadata=set_metadata, size=size,norm=norm)

            # get marker
            self.statusbar.showMessage("  Getting markers...")
            if not (isinstance(marker,np.ndarray) or isinstance(marker,list)):
                try:
                    marker=[self.get_metadata("Marker x "+detector,viz=True),self.get_metadata("Marker y "+detector,viz=True)]
                    marker[0]+1
                except Exception as e:
                    self.statusbar.showMessage("Error: No markers defined.")
                    marker=[int(image.shape[0]/2), int(image.shape[1]/2)]
        else:
            self.statusbar.showMessage("  Getting raw image...")
            image = self.get_image(detector=detector)
            marker = [int(image.shape[0]/2), int(image.shape[1]/2)]
            
        # lineouts at marker
        self.statusbar.showMessage("  Summing over linewidth...")
        line_h = np.sum(image[marker[0]-int(linewidth/2):marker[0]+int(linewidth/2),:],axis=0)
        line_v = np.sum(image[:,marker[1]-int(linewidth/2):marker[1]+int(linewidth/2)],axis=1)
            
        # backround correction
        self.statusbar.showMessage("  Estimating background...")
        offset=linewidth
        line_hb_plus = np.sum(image[marker[0]+int(linewidth/2):marker[0]+int(3*linewidth/2),:],axis=0)
        line_vb_plus = np.sum(image[:,marker[1]+int(linewidth/2):marker[1]+int(3*linewidth/2)],axis=1)
        line_hb_minus = np.sum(image[marker[0]-int(3*linewidth/2):marker[0]-int(linewidth/2),:],axis=0)
        line_vb_minus = np.sum(image[:,marker[1]-int(3*linewidth/2):marker[1]-int(linewidth/2)],axis=1)

        line_hb = line_hb_plus
        line_hb[marker[0]:] = line_hb_minus[marker[0]:]
        line_vb = line_vb_plus
        line_vb[marker[0]:] = line_vb_minus[marker[0]:]
            
        if bg_correction:
            self.statusbar.showMessage("  Subtracting background...")
            line_h -= line_hb
            line_v -= line_vb
        
        if detector == "Zyla":
            line_hb -= np.min(line_h[150:300])
            line_h -= np.min(line_h[150:300])
            line_vb -= np.min(line_v[150:300])
            line_v -= np.min(line_v[150:300])
        
        if norm == "max":
            self.statusbar.showMessage("  Normalizing to maximum...")
            line_hb = line_hb/np.max(line_h)
            line_h  = line_h/np.max(line_h)
            line_vb = line_vb/np.max(line_v)
            line_v  = line_v/np.max(line_v)
                       
        self.statusbar.showMessage("Done with lineprofiles")
        return line_h, line_v, line_hb, line_vb,q_h,q_v

    def get_darkfield_image(self,detector=''):
        if hasattr(self,"metadata"):
            darkfield_number = self.get_metadata("darkfield")
            if darkfield_number != None:
                return self.runs[darkfield].mean(detector=detector) #!!!!!
            else:
                return 0
        else:
            return 0
    
    def get_flatfield_image(self,detector=''):
        if hasattr(self,"metadata"):
            flatfield_number = self.get_metadata("flatfield")
            if flatfield_number != None:
                return self.runs[flatfield].mean(detector=detector) #!!!!!
            else:
                return 1
        else:
            return 1
    
    def rotate_around_center(self,a,deg,center=None,crop=False):
        try:
            a[np.where(a==0)]+=1e-9
        except Exception as e:
            None
        sa = a.shape
        if center==None:
            center=[int(sa[0]/2),int(sa[1]/2)]
            
        #1. enlarge
        c=np.zeros((int(np.max([center[0],sa[0]-center[0]])*2*2**.5),int(np.max([center[1],sa[1]-center[1]])*2*2**.5)))
        sc = c.shape
        c[int(sc[0]/2)-center[0]:int(sc[0]/2)+(sa[0]-center[0]),int(sc[1]/2)-center[1]:int(sc[1]/2)+(sa[1]-center[1])] = a
        #c[int(sc[0]/2)-5:int(sc[0]/2)+5,int(sc[1]/2)-5:int(sc[1]/2)+5] = 250

        #2. rotate
        c = rotate(c,deg,resize=False,order=0)
        
        #3. crop
        if crop:
            x = np.sum(c,axis=1)
            x_min = np.min(np.where(x != 0))
            x_max = np.max(np.where(x != 0))

            y = np.sum(c,axis=0)
            y_min = np.min(np.where(y != 0))
            y_max = np.max(np.where(y != 0))
            c = c[x_min:x_max,y_min:y_max]

            #4. new center
            new_center=[int(sc[0]/2)-x_min,int(sc[1]/2)-y_min]
            #c[new_center[0]-15:new_center[0]+15,new_center[1]-15:new_center[1]+15] = 250
        else:
            sc = c.shape
            new_center=[int(sc[0]/2),int(sc[1]/2)]
        
        return c,new_center

    def get_processed_image(self, cache_only = False, recalc = False, detector='', smooth = None, center=None, marker=None, deg=None, set_metadata=True, size=None, show_markers=True,linewidth=16,norm="I0",minmax=False):
        """
            **Post-processor for raw data**
            
            This method does the following:
            
            1. **darkfielding and flatfielding:** subtract the last event that is marked as shot_type ``darkfield``, and divide by the last event that is marked as shot_type ``flatfield``. The latter is read locally, hence in the :ref:`settings <settings>` the caching option or source=local must be set.
            2. **apply thresholds** 
            3. **detector calibration**
            4. **unwrapping (only for detector JF4)**
            5. **rotation**
            6. **smoothing**
            7. **cropping**
            8. **caching**
            9. **normalization**
            10. **adding markers**
            
            :param recalc: reprocess, even if a local cache exists
            :paramminmax: *bool* (default: ``False``): return also the min and max vaules of the processed image normalized to I0
        """
        self.statusbar.showMessage("------------------------------- Start processing -------------------------------")
        if detector == '': detector = self.settings["detector"]
        fn = self.detector_filename(self.filename,detector,subdir = "processed")

        if hasattr(self,"metadata"):
            self.statusbar.showMessage("Checking for cached processed image in memory for event " + str(self.event_number))
            if not hasattr(self,"_processed_image_"+detector) or recalc:
                self.statusbar.showMessage("...not in memory. Now checking for cached processed image on disk for event " + str(self.event_number) + " at " + str(fn) + '...')
                if not recalc and os.path.exists(fn):
                    self.statusbar.showMessage("Reading processed image from disk cache at " + str(fn) + '...')
                    img_ = self.get_image(detector=detector,cache_only=True)
                    if self.settings["fileType"] == "txt":
                        image = np.loadtxt(fn)
                    if self.settings["fileType"] == "npy":
                        image = np.load(fn)
                    if self.settings["fileType"] == "npz":
                        image = np.load(fn)
                        image = image.f.arr_0
                    if self.settings["fileType"] in ["tiff","tif"]:
                        image = tifffile.imread(fn)
                else:
                    self.statusbar.showMessage("Processing image for " + detector+" because: "+str(hasattr(self,"_processed_image_"+detector))+ str(recalc) )
                    image = self.get_image(detector=detector)
                    
                    # 1. darkfielding and flatfielding
                    try:
                        image -= self.get_darkfield_image(detector=detector)
                        self.statusbar.showMessage("Darkfielding applied.")
                    except Exception as e:
                        self.statusbar.showMessage("No darkfielding applied.")
                        self.statusbar.showMessage(e)

                    if detector != "JF4" and rosa_connected:
                        # for the JF4 SAXS detector, rosahami will do the job
                        try:
                            image = image/self.get_flatfield_image(detector=detector)
                            self.statusbar.showMessage("Flatfielding applied.")                    
                        except Exception as e:
                            self.statusbar.showMessage("No flatfielding applied.")
                            self.statusbar.showMessage(e)

                    # 2. Apply thresholds
                    # will not work properly for trains@XFEL
                    self.statusbar.showMessage("2. Apply thresholds")
                    try:
                        fl=image<self.settings['threshold_lower_'+detector]
                        image[fl]=0#float(self.settings['threshold_lower_'+detector])
                        self.statusbar.showMessage("Lower threshold " + str(self.settings['threshold_lower_'+detector]) +" applied.")
                    except Exception as e:
                        self.statusbar.showMessage("No lower threshold applied.")
                        self.statusbar.showMessage(e)
                    try:
                        fl=image>self.settings['threshold_upper_'+detector]
                        image[fl]=float(self.settings['threshold_upper_'+detector])
                        self.statusbar.showMessage("Upper threshold " + str(self.settings['threshold_upper_'+detector]) +" applied.")
                    except Exception as e:
                        self.statusbar.showMessage("No upper threshold applied.")
                        self.statusbar.showMessage(e)

                    # 4. SAXS specific things: correct perspective
                    self.statusbar.showMessage("4. SAXS specific things: correct perspective")
                            
                    if detector == "JF4":
                        rosa_error = 0
                        if rosa_connected:
                            try:
                                #flatfield_number = self.get_metadata("flatfield") or 65
                                #flatfield = self.settings["filePrefix"] + str(flatfield_number) + self.settings["fileEnd"] + self.settings["fileMustHave"]
                                flatfield = 'rosahami-unwrap-map-XFEL'
                                image,qx,qy,deg_SAXS = rosa.get_rosahami_unwrapped_ff_image(image, flatfield = flatfield, verbose=False)
                                if not isinstance(center,(np.ndarray,list)):
                                    try:
                                        center=[self.get_metadata("Center x "+detector,viz=True),self.get_metadata("Center y "+detector,viz=True)]
                                        center[0]+1
                                    except Exception as e:
                                        center = [int(image.shape[0]/2), int(image.shape[1]/2)]
                            except:
                                rosa_error = 1
                        if not rosa_connected or rosa_error == 1:
                            # 4a. correct perspective
                            if not isinstance(center,(np.ndarray,list)):
                                try:
                                    center=[self.get_metadata("Center x "+detector,viz=True),self.get_metadata("Center y "+detector,viz=True)]
                                    center[0]+1
                                except Exception as e:
                                    center = [int(image.shape[0]/2), int(image.shape[1]/2)]
                            try:
                                deg_SAXS = self.get_metadata("stretch rot "+detector,viz=True)
                                if deg_SAXS == None: deg_SAXS = 0
                                self.statusbar.showMessage("Using poor mans unwrapping based on just stretching the image")
                                image, center = self.rotate_around_center(image,deg_SAXS,center,crop=False)
                                stretch_SAXS = self.get_metadata("stretch fac "+detector,viz=True)
                                a=image.shape
                                image = resize(image,(int(a[0]*stretch_SAXS),a[1]))/stretch_SAXS
                                a=image.shape
                                center[0] = int(center[0]*stretch_SAXS)
                            except Exception as e:
                                self.statusbar.showMessage("Failed in this poor men's approach:")
                                self.statusbar.showMessage(e)

                    if 'qx' in locals():
                        dqx = qx[1]-qx[0]
                    else:
                        dqx = 1
                    if 'qy' in locals():
                        dqy = qy[1]-qy[0]
                    else:
                        dqy = 1
                                            
                    # 3. detector calibration
                    self.statusbar.showMessage("3. detector calibration")
                    try:
                        calib = self.settings["calibration_"+detector]
                    except Exception as e:
                        calib = 1
                    image = image/(1.*calib)

                    # 5. rotation around origin center                    
                    self.statusbar.showMessage("5. rotation around origin center")
                    try:
                        if deg == None:
                            try:
                                deg = self.get_metadata("rotation "+detector,viz=True)
                            except Exception as e:
                                deg = 0
                        if detector == "JF4":
                            deg_ = deg - deg_SAXS
                        else:
                            deg_ = deg
                    except:
                        None
                        
                    try:
                        #if deg_ != 0:
                        image, center_ = self.rotate_around_center(image,deg_,center)
                    except:
                        center_ = [int(image.shape[0]/2), int(image.shape[1]/2)]
                    #else:
                    #    center_ = [center[0]*1,center[1]*1]

                    # 6. smoothing
                    self.statusbar.showMessage("6. smoothing")
                    if smooth != None:
                        image = gaussian_filter(image,smooth)

                    # Normalization: later, after caching in object attribute

                    # 7. cropping
                    a = image.shape

                    if not (isinstance(size,np.ndarray) or isinstance(size,list)):
                        try:
                            size=[self.get_metadata("size x "+detector,viz=True),self.get_metadata("size y "+detector,viz=True)]
                            size[0] + 1
                        except Exception as e:
                            size=a

                    image = image[center_[0]-int(size[0]/2):center_[0]+int(size[0]/2),center_[1]-int(size[1]/2):center_[1]+int(size[1]/2)]
                        #marker[0] = int((size[0])/2)
                        #print("h",center,a,size,marker)
                        #marker[1] = int((size[1])/2)

                    if not isinstance(marker,np.ndarray): 
                        try:
                            marker=[self.get_metadata("Marker x "+detector,viz=True),self.get_metadata("Marker y "+detector,viz=True)]
                            marker[0]+1
                        except Exception as e:
                            marker = [int(image.shape[0]/2), int(image.shape[1]/2)]

                    # write metadata markers
                    if set_metadata:
                        try:
                            self.set_metadata("Marker x "+detector,int(marker[0]),viz=True)
                            self.set_metadata("Marker y "+detector,int(marker[1]),viz=True)
                            #self.set_metadata("Center x "+detector,int(center[0]),viz=True)
                            #self.set_metadata("Center y "+detector,int(center[1]),viz=True)
                            self.set_metadata("rotation "+detector,deg,viz=True)
                            self.set_metadata("size x "+detector,int(size[0]),viz=True)
                            self.set_metadata("size y "+detector,int(size[1]),viz=True)
                            self.set_metadata("dq x "+detector,dqx,viz=True)
                            self.set_metadata("dq y "+detector,dqy,viz=True)
                        except:
                            None
    #                #this one could display and get new markers and some more rotation
    #                # ... plt....
    #                # new_marker = ...
    #                # more_rotation = ...
    #                
    #                #then back-transform
    #                new_center = marker_to_center(image,image_rotated,marker)
    #                deg = deg+more_rotation

                    #8. caching
                    setattr(self,"_processed_image_"+detector, image*1)
                    if self.settings["cache"]:
                        try:
                            self.statusbar.showMessage("Caching processed image of event " + str(self.event_number) + ' at '+fn)
                            try:
                                Path(local[0:find_all(fn,'/')[-1]]).mkdir(parents=True, exist_ok=True) #os.sep
                            except:
                                None
                            if self.settings["fileType"] == "npz":
                                np.savez_compressed(fn,image)
                            if self.settings["fileType"] == "npy":
                                np.save(fn,image)
                            if self.settings["fileType"] == "tif":
                                tifffile.imwrite(fn,image)
                            if self.settings["fileType"] == "txt":
                                np.savetxt(fn,image)
                        except Exception as e:
                            self.statusbar.showMessage(e)
                            if self.debug: raise


                # === 9. Normalization ===
                self.statusbar.showMessage("9. Normalization")
                #if detector == "Zyla":
                #    image -= np.min(image[150:,150:])
                    
                if norm == "I0" or minmax:
                    try:
                        #I0 = self.get_metadata("I0",viz=True)
                        image = image/ self.I0
                    except Exception as e:
                        self.statusbar.showMessage(e)
                        self.statusbar.showMessage("...so we continue without normalization to I0")
                    
                if minmax: 
                    min_=np.min(image)
                    max_=np.max(image)
                    if norm != "I0" and norm != "max":
                        try:
                            image = image * self.I0
                        except Exception as e:
                            self.statusbar.showMessage(e)
                            self.statusbar.showMessage("...so we continue without normalization to I0")

                if norm == "max":
                    image = image/np.max(image)
                    
                if norm =="":
                    norm = "photons"
                
                # 10. Show markers
                if not isinstance(marker,np.ndarray): 
                    try:
                        marker=[self.get_metadata("Marker x "+detector,viz=True),self.get_metadata("Marker y "+detector,viz=True)]
                        marker[0]+1
                    except Exception as e:
                        marker = [int(image.shape[0]/2), int(image.shape[1]/2)]

                self.statusbar.showMessage("10. Adding markers if requested...")
                if show_markers:
                    self.statusbar.showMessage("  ...yes, adding markers to image")
                    max__ = np.max(image)

                    image[marker[0]-int(linewidth/2)-1:marker[0]-int(linewidth/2)+1,marker[1]-linewidth*2:marker[1]+linewidth*2] = max__
                    image[marker[0]-linewidth*2:marker[0]+linewidth*2,marker[1]-int(linewidth/2)-1:marker[1]-int(linewidth/2)+1] = max__
                    image[marker[0]+int(linewidth/2)-1:marker[0]+int(linewidth/2)+1,marker[1]-linewidth*2:marker[1]+linewidth*2] = max__
                    image[marker[0]-linewidth*2:marker[0]+linewidth*2,marker[1]+int(linewidth/2)-1:marker[1]+int(linewidth/2)+1] = max__

                # 11. Calculating q:
                self.statusbar.showMessage("11. Calculating q...")
                
                dqx=self.get_metadata("dq x "+detector,viz=True)
                dqy=self.get_metadata("dq y "+detector,viz=True)   
                if dqx == None:
                    dqx=1
                if dqy == None:
                    dqy=1
                y0 = marker[0]
                y = np.arange(image.shape[1])
                q_h = y-y0
                q_h = q_h * dqy

                x0 = marker[1]
                x = np.arange(image.shape[0])
                q_v = x-x0
                q_v = q_v * dqx

                self.statusbar.showMessage("------------------------------- Done processing -------------------------------")
                if minmax: return image, q_h, q_v, min_,max_
                return image, q_h, q_v
            else:
                if not cache_only:
                    self.statusbar.showMessage("Using the cached processed image, attribute "+"_processed_image_"+detector)
                    image = getattr(self,"_processed_image_"+detector)*1
                
                    self.statusbar.showMessage("  Calculating q...")
                    if not isinstance(marker,np.ndarray): 
                        try:
                            marker=[self.get_metadata("Marker x "+detector,viz=True),self.get_metadata("Marker y "+detector,viz=True)]
                            marker[0]+1
                        except Exception as e:
                            marker = [int(image.shape[0]/2), int(image.shape[1]/2)]
                    try:
                        dqx=self.get_metadata("dq x "+detector,viz=True)
                        dqy=self.get_metadata("dq y "+detector,viz=True)                    
                    except Exception as e:
                        dqx=1
                        dqy=1
                    y0 = marker[1]
                    y = np.arange(image.shape[1])
                    q_h = y-y0
                    q_h = q_h * dqy

                    x0 = marker[0]
                    x = np.arange(image.shape[0])
                    q_v = x-x0
                    q_v = q_v * dqx

                    # === 9. Normalization ===
                    #if detector == "Zyla":
                    #    image -= np.min(image)

                    self.statusbar.showMessage("Normalizing to "+norm+"...")
                    if norm == "I0" or minmax:
                        #I0 = self.get_metadata("I0",viz=True)
                        image = image/ self.I0

                    if minmax: 
                        min_=np.min(image)
                        max_=np.max(image)
                        if norm != "I0" and norm != "max":
                            image = image * self.I0
                    
                    if norm == "max":
                        image = image/np.max(image)

                    # 8. Show markers
                    if show_markers:
                        self.statusbar.showMessage("Adding markers to image")
                        max__ = np.max(image)
                        image[marker[0]-int(linewidth/2)-1:marker[0]-int(linewidth/2)+1,marker[1]-linewidth*2:marker[1]+linewidth*2] = max__
                        image[marker[0]-linewidth*2:marker[0]+linewidth*2,marker[1]-int(linewidth/2)-1:marker[1]-int(linewidth/2)+1] = max__
                        image[marker[0]+int(linewidth/2)-1:marker[0]+int(linewidth/2)+1,marker[1]-linewidth*2:marker[1]+linewidth*2] = max__
                        image[marker[0]-linewidth*2:marker[0]+linewidth*2,marker[1]+int(linewidth/2)-1:marker[1]+int(linewidth/2)+1] = max__

                    self.statusbar.showMessage("------------------------------- Done processing -------------------------------")
                    if minmax: return image,q_h,q_v,min_,max_
                    return image, q_h, q_v
        else:
            self.statusbar.showMessage("Returning unprocessed image due to missing metadata labbook.")
            return self.get_image(detector = detector), 1, 1
       
    
    def get_image_from_API(self,detector=''):
        try:
            self.statusbar.showMessage("Obtaining new event via API: " + str(self.event_number) + '...')
            if detector == '': detector = self.settings["detector"]
            #try:
            run = em.open_run(self.settings["proposal"], self.event_number,data='all')
            try:
                th = self.settings["threshold_lower_"+detector]                                                                                                 
            except Exception as e:
                th=-1
    
            # === get trainID ===
            try:
                if self.is_shot_type("mainshot") and detector!="Zyla":
                    self.statusbar.showMessage("getting trainID...")
                    trainId=em.get_shot_trainId(run) #Getting the trainId when the laser was shot from IPM
                    self.statusbar.showMessage("trainID = "+str(trainId))
                        #self.set_filename(fn)
                else:
                    trainId = -1
            except:
                trainId = -1

            # === get I0 from API ===
            if not hasattr(self,"I0"):
                self.statusbar.showMessage("I0 not existing yet. Obtaining via API...")
                if self.is_shot_type("mainshot"):
                    try:
                        I0 = em.get_shot_I(run,average=False)
                    except Exception as e:
                        self.statusbar.showMessage(e)
                        I0 = 1
                else:
                    try:
                        I0 = em.get_shot_I(run,trainId=-1,average=False)
                    except Exception as e:
                        self.statusbar.showMessage(e)
                        I0 = 1
                self.set_metadata("I0",I0,viz=True)
                self.statusbar.showMessage("I0 set in labbook to micro J"+str(I0))
                try:
                    I0 = I0*6.2e9/self.settings["calibration_JF4"]
                except:
                    None
            else:
                I0 = self.I0
#                try:
#                    I0 = self.get_metadata("I0",viz=True)
#                except Exception as e:
#                    self.statusbar.showMessage(e)
#                    if self.debug: raise
#                    I0 = 1
                
            try:
                self.statusbar.showMessage("New event " + str(self.event_number) + ' has I0 photons ' + str(I0))
                setattr(self,"I0",I0)
            except Exception as e:
                self.statusbar.showMessage(e)
                if self.debug: raise
                self.statusbar.showMessage("Error setting I0! You will not be able to normalize outputs by I0!")

            image=em.get_image(run,
                               self.event_number,
                               detector, # e.g. "JF1"
                               recalc=1,
                               trainId=trainId,
                               threshold_lower=th,
                               average = False,
                               save=False
                              )
            if self.settings["cache"]:
                fn = self.detector_filename(self.filename,detector)
                self.statusbar.showMessage("Caching new event " + str(self.event_number) + ' at '+fn)
                if self.settings["fileType"] == "npz":
                    np.savez_compressed(fn,image)
                if self.settings["fileType"] == "npy":
                    np.save(fn,image)
                if self.settings["fileType"] == "tif":
                    tifffile.imwrite(fn,image)
                if self.settings["fileType"] == "txt":
                    np.savetxt(fn,image)
                self.statusbar.showMessage("Done caching new event " + str(self.event_number))

            self.statusbar.showMessage("Done obtaining new event via API: " + str(self.event_number) + '...')
            return image
        except Exception as e:
            self.statusbar.showMessage(e)
            if self.debug: raise
            self.statusbar.showMessage("ERROR while obtaining new event via API: " + str(self.event_number) + ' for '+detector+'...')
            return None

    def get_image_from_SFTP(self,detector=''):
        try:
            fn = self.detector_filename(self.filename,detector)
            fn_remote = self.detector_filename(self.filename_remote,detector)
            self.statusbar.showMessage("Obtaining new run from " + str(fn_remote) + '...' + str(fn))
            self.runs.ssh.get_file(fn_remote,fn)
            self.statusbar.showMessage("Obtained new run from " + str(fn_remote) + '...')
            image = self.get_image_from_local_file(detector=detector)
            if not self.settings["cache"]:
                # we have to remove the local file, since caching is not wanted:
                if os.path.exists(self.filename):
                    os.remove(self.filename)
            else:
                self.statusbar.showMessage("New event " + str(self.event_number) + ' cached at '+self.filename)

            try:
                I0 = self.get_metadata("I0",viz=True)
                try:
                    I0 = I0*6.2e9/self.settings["calibration_JF4"]
                except:
                    None
            except Exception as e:
                self.statusbar.showMessage(e)
                if self.debug: raise
                I0 = 1
            
            try:
                self.statusbar.showMessage("New event " + str(self.event_number) + ' has I0 ' + str(I0))
                setattr(self,"I0",I0)
            except Exception as e:
                self.statusbar.showMessage(e)
                if self.debug: raise
                self.statusbar.showMessage("Error setting I0! You will not be able to normalize outputs by I0!")
            
            self.statusbar.showMessage("Done obtaining new run from " + str(fn_remote) + '...')
            return image
        except Exception as e:
            self.statusbar.showMessage("ERROR while obtaining new run from " + str(fn_remote) + '...')
            return None

    def detector_filename(self,fn,detector,subdir=""):
        if detector != '':
            start = fn.find(self.settings["detector"])
            end = start + len(self.settings["detector"])
            fn = fn[0:start] + detector + fn[end:]
        if subdir != '':
            start = find_all(fn,"/")[-1]
            fn = fn[0:start]+"/"+subdir+"/"+fn[start:]
        return fn
        
    def get_image_from_local_file(self,detector=''):
        try:
            fn = self.detector_filename(self.filename,detector)
            self.statusbar.showMessage("Reading new run from " + str(self.filename) + '...')
            if self.settings["fileType"] == "txt":
                image = np.loadtxt(fn)
            if self.settings["fileType"] == "npy":
                image = np.load(fn)
            if self.settings["fileType"] == "npz":
                image = np.load(fn)
                image = image.f.arr_0
            if self.settings["fileType"] in ["tiff","tif"]:
                image = tifffile.imread(fn)

            try:
                I0 = self.get_metadata("I0",viz=True)
                try:
                    I0 = I0*6.2e9/self.settings["calibration_JF4"]
                except:
                    None
            except Exception as e:
                self.statusbar.showMessage(e)
                if self.debug: raise
                I0 = 1
            
            try:
                self.statusbar.showMessage("New event " + str(self.event_number) + ' has I0 ' + str(I0))
                setattr(self,"I0",I0)
            except Exception as e:
                self.statusbar.showMessage(e)
                if self.debug: raise
                self.statusbar.showMessage("Error setting I0! You will not be able to normalize outputs by I0!")
                
            self.statusbar.showMessage("Done reading new event " + str(self.event_number) + ' in run ' + str(self.run.run_number) + '.')

            return image
        except Exception as e:
            self.statusbar.showMessage(e)
            self.statusbar.showMessage("Error: Could not read new event " + str(self.event_number) + ' in run ' + str(self.run.run_number) + ' from local source.')
            if self.debug: raise
        
            return None
        
    def set_metadata(self,key,data,viz=False):
        try:
            if self.settings["fileSeparator"] == '' or self.settings["API"]:
                #flat numbering
                return self.metadata.set_data(key,data,self.event_number,viz=viz)
            else:
                return self.metadata.set_data(key,data,self.run.run_number, event_number= self.event_number,viz=viz)  
        except Exception as e:
            self.statusbar.showMessage(e)
            if self.debug: raise

    def get_metadata(self,key,viz=False):
        try:
            if self.settings["fileSeparator"] == '' or self.settings["API"]:
                #flat numbering
                return self.metadata.get(key,self.event_number,viz=viz)
            else:
                return self.metadata.get(key,self.run.run_number, event_number= self.event_number,viz=viz)  
        except Exception as e:
            self.statusbar.showMessage(e)
            if self.debug: raise

    def get_metadata_unit(self,key,viz=False):
        try:
            return self.metadata.get_unit(key,viz=viz)
        except Exception as e:
            self.statusbar.showMessage(e)
            if self.debug: raise
    
    def open_in_imageJ(self,shot_type=None,detector='',processed=True,log=True):
        self.runs.connect_imageJ()
        if proc:
            image, q_h,q_v = self.get_processed_image(detector=detector)
        else:
            image = self.get_image(detector=detector)
        if self.shot_type != None:
            image_url = "run_"+str(self.run.run_number)+'-event_'+str(self.event_number)+'_'+self.shot_type+'_'+str(self.get_metadata("delay"))+str(self.get_metadata_unit("delay"))+'_'+detector+'.tif'
        else:
            image_url = "run_"+str(self.run.run_number)+'-event_'+str(self.event_number)+'_'+detector+'.tif'
        tifffile.imwrite(image_url,image)
        jimage = self.runs.ij.io().open(image_url)
        self.runs.ij.ui().show(jimage)
        os.remove(image_url)
        if log:
            macro = "run(\"Abs\"); run(\"Add...\", \"value=1e-9\"); run(\"Log\"); run(\"Fire\");run(\"Brightness/Contrast...\");"
        else:
            macro = "run(\"Abs\"); run(\"Add...\", \"value=1e-9\"); run(\"Fire\");run(\"Brightness/Contrast...\");"
        try:
            self.runs.ij.py.run_macro(macro)
        except Exception as e:
            self.statusbar.showMessage(e)
            if self.debug: raise

class Metadata():
    def __init__(self,
                 keyfile = 'google_access.json',
                 sheet = 'elog_SACLA_2021B8043', #None
                 worksheet = 0,
                 vizsheet = 1,
                 head = 1
                ):
        
        self.debug = False
        
        self._scope = ['https://spreadsheets.google.com/feeds','https://www.googleapis.com/auth/drive']
                        
        if keyfile != None:
            self.set_keyfile(keyfile)

        self._worksheet = worksheet
        self._vizsheet = vizsheet
        self._sheet = sheet
            
        #if worksheet != None and vizsheet == None:
        self.set_worksheet(worksheet)

        if sheet != None:
            self.set_sheet(sheet)


        if head != None:
            self.head = head
            
        self._read_again = True
            
            
        class Statusbar():
            def __init__(self):
                None
            def showMessage(self,text):
                if verbose:
                    print(text)
                else:
                    None
        self.statusbar = Statusbar()
            
    def set_head(self,head):
        if self._head != head: self._read_again = True
        self._head = head

    def set_keyfile(self,keyfile):
        self._creds = ServiceAccountCredentials.from_json_keyfile_name(keyfile, self._scope)
        
    def set_sheet(self,sheet):
        self._sheet = sheet
        self._get_sheet_instance()

    def set_worksheet(self,worksheet, vizsheet=None):
        self._worksheet = worksheet
        if vizsheet != None:
            self._vizsheet = vizsheet
        self._get_sheet_instance()
        
    def _get_sheet_instance(self):
        while True:
            try:
                # authorize the clientsheet 
                client = gspread.authorize(self._creds)
                # get the instance of the Spreadsheet
                sheet = client.open(self._sheet)
                # get the required sheet of the Spreadsheet
                self._sheet_instance = sheet.get_worksheet(self._worksheet)
                if self._vizsheet != None:
                    self._vizsheet_instance = sheet.get_worksheet(self._vizsheet)
                return self._sheet_instance
            except Exception as e:
                print("Error getting the google sheet instance!")
                #return None

    def update(self):
        try:
            #read google spreadsheet, if already open:
            # get all the records of the data
            records_data = self._sheet_instance.get_all_records(head=self.head)#[2:]
            records_data_viz = self._vizsheet_instance.get_all_records(head=self.head)#[2:]
        except Exception as e:
            # else open the spreadsheet first
            records_data = self._get_sheet_instance().get_all_records(head=self.head)#[2:]
            records_data_viz = self._vizsheet_instance.get_all_records(head=self.head)#[2:]
        # convert the json to dataframe
        self.records_df = pd.DataFrame.from_dict(records_data)
        self.records_df_viz = pd.DataFrame.from_dict(records_data_viz)
        self._read_again = False
        
    def get_unit(self,key,viz=False):
        if viz:
            return self.records_df_viz.at[0,key]
        else:
            return self.records_df.at[0,key]

    def get_run_for_event(self,run_number):
        try:
            my_shot_type = self.get("shot_type",run_number) 
            if not my_shot_type in ["preshot","mainshot","postshot"]: return run_number
            row_self = np.where(self.records_df['run_number'] == run_number)[0][0] # find the line of this run_number
            row = row_self
            while self.records_df.at[row,'shot_type'] == "postshot":
                row -= 1
            if not self.records_df.at[row,'shot_type'] in ["preshot","mainshot","postshot"]: return self.records_df.at[row,'run_number']
            #now row is the first line before the postshots, this might already be the start of the current run! now test if the line before is a postshot or not in list
            try:
                while self.records_df.at[row,'shot_type'] in ["mainshot"]:
                    row -= 1
            except Exception as e:
                None
            try:
                while self.records_df.at[row,'shot_type'] in ["preshot"]:
                    row -= 1
            except Exception as e:
                None
            return self.records_df.at[row + 1,'run_number']
        except:
            print("Error! main for run not found in metadata get_run_for_event")
            return run_number
        
    def set_data(self,key, data, run_number, event_number=None, viz=False):
        try:
            if viz:
                ws = self._vizsheet_instance
                df = self.records_df_viz
            else:
                ws = self._worksheet_instance
                df = self.records_df

            # Create empty dataframe
            df_ = pd.DataFrame()

            #find row:
            if event_number == None:
                #flat numbering
                row = np.where(df['run_number'] == run_number)[0][0]
            else:
                row = np.where(df['run_number'] == run_number)[0][0] # find the first line of the run on labbook
                row0 = row + 0
                while row-row0<20:
                    # look in this and the next lines for the desired event number
                    if df.at[row,'Event #'] == event_number:
                        break
                    else:
                        row += 1
            #find col:
            col = df.columns.get_loc(key)

            #update the first sheet with df, starting at cell B2. 
            "set",ws.update_cell(row+2,col+1,str(data))
            return True
        except Exception as e:
            #print(e)
            return False

    def get(self,key, run_number, event_number=None, viz=0):
        run_number_=run_number
        try:
            if viz:
                df = self.records_df_viz
            else:
                df = self.records_df
            if event_number == None:
                #flat numbering
                while True:
                    try:
                        this = df.set_index('run_number')[key].to_dict()[run_number]
                    except Exception as e:
                        this = ''
                    if this != '': 
                        return this
                    else:
                        if run_number == 0:
                            if viz == 1: 
                                return None
                            else:
                                #print("trying the other ws...")
                                return self.get(key, run_number_, event_number=event_number, viz=1-viz)
                        run_number -= 1
            else:
                row = np.where(df['run_number'] == run_number)[0][0] # find the first line of the run on labbook
                row0 = row + 0
                while row-row0<20:
                    # look in this and the next lines for the desired event number
                    if df.at[row,'Event #'] == event_number:
                        this = df.at[row,key]
                    else:
                        row += 1

                while True:
                    #if the key metadata is not found go back lines, until it is found
                    if this != '': 
                        return this
                    else:
                        row -= 1
                        if run_number == 0:
                            if viz == 1: 
                                return None
                            else:
                                #print("trying the other ws...")
                                return self.get(key, run_number_, event_number=event_number, viz=1-viz)
                        this = df.at[row,key]
        except Exception as e:
            #print(e)
            None
            

#try:
metadata_XFEL2818 = Metadata(keyfile = 'google_access.json', # The google API access token in json format
                                  sheet = 'Logbook EuXFEL 2818', # The name of the google sheet
                                  worksheet = 0, # the shotsheet number (0 is the first)
                                  vizsheet = 1, # the sheet where the vizualization parameters are stored
                                  head = 1 # number of leading header lines, EXcluding the unit line.
                                 )
#except Exception as e:
#    print(e)
#    print("Metadata could not be connected to google!")
    
# settings for generator of overviews and data in share:
settings={}
settings["filePrefix"] = 'run_' # The string in front of the run_number, or, if there is none, one of '\' or '/' depending on your operating system
# set eactly 1 to true:
settings["local"] = False # set to True if you want to read the data from a local source
settings["API"] = True # set to True to read from the mmm API
settings["SFTP"] = False # set to True if you want to reat from SFTP

settings["proposal"] = 2818 # 2621 # for testing 2818 # 

settings["cache"] = True # store image copies locally
settings["cacheMemory"] = 10 # store images in memory
settings["remoteDataPath"] = "/gpfs/exfel/exp/HED/202201/p002818/usr/Shared/data/"
settings["remoteOutputPath"] = "/gpfs/exfel/exp/HED/202201/p002818/usr/Shared/overviews/"
settings["localDataPath"] = "/gpfs/exfel/exp/HED/202201/p002818/usr/Shared/data/" # only used for local=True or for cache
settings["localOutputPath"] = "/gpfs/exfel/exp/HED/202201/p002818/usr/Shared/overviews/"

settings["fileSeparator"] = "" # separator between run_number and event_number
settings["fileType"] = "tif"
settings["fileEnd"] = "-" # character string ending the numbers
settings["fileMustHave"] = "JF4" # only consider images containing this string
#for each detector, specify:
settings["detector"] = "JF4" # The string defined here will be replaced by the detector string during runtime and defines the standard detector
settings['threshold_lower_JF4'] = 6 # a lower value below which the detector values are set to settings['threshold_lower_JF4']. Jungfrau: [keV]; JF4: SAXS
#settings['threshold_upper_JF4'] = 10 # an upper value below which the detector values are set to settings['threshold_upper_JF4']. Jungfrau: [keV]; JF4: SAXS
settings["calibration_JF4"] = 8 # how much detector value is one unit?
settings["unit_JF4"] = "photons" # unit string

settings["imageNorm"] = "XFEL energy" # the key in the shotsheet where a normalizing amplitude is stored            

settings["start_run"] = 1 # first run to consider
settings["end_run"] = 29999 # last run to consider

settings["slack_token"]="xoxb-3172548063763-3174640001093-ezaj0pPuXFYkHcKn35U6Fd0S"
settings_XFEL2818_share=deepcopy(settings)

# settings for XFEL experiment in user directory on Maxwell:
settings["remoteDataPath"] = "/gpfs/exfel/exp/HED/202201/p002818/usr/Shared/data/"
settings["remoteOutputPath"] = "/gpfs/exfel/exp/HED/202201/p002818/usr/Shared/overviews/"
settings["localDataPath"] = "./data/" # only used for local=True or for cache
settings["localOutputPath"] = "./overviews/"
settings_XFEL2818_user = deepcopy(settings)

# settings for local experiment
settings["local"] = True # set to True if you want to read the data from a local source
settings["API"] = False # set to True to read from the mmm API
settings["SFTP"] = False # set to True if you want to reat from SFTP
settings_XFEL2818_local = deepcopy(settings)

# settings for ssh to XFEL experiment
settings["local"] = False # set to True if you want to read the data from a local source
settings["API"] = False # set to True to read from the mmm API
settings["SFTP"] = True # set to True if you want to reat from SFTP
settings["hostname"] = "max-display.desy.de"

settings["username"] = None # change this to your XFEL username
settings["password"] = None # optional: provide your xfel password
settings_XFEL2818_sftp = deepcopy(settings)



