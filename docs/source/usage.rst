First steps
===========

Installation
------------

Currently, you have to download the repository and solve the dependencies manually:

1. fork
2. Clone the repo to your local machine (or to [maxwell](https://max-jhub.desy.de/user/kluget/lab/tree/GPFS/exfel/exp/HED/202201/p002818/usr/Software/overview)), using git:

    ``clone https://gitlab.hzdr.de/kluget/run_and_event_viewer.git

3. (optional) Switch to the ``dev`` or ``nightly`` branch, to get the latest nightly fixes

4. Create a new branch, e.g. ``[dev-your name]``

    ``git checkout -b ``[name_of_your_new_branch]``
    ``git push origin [name_of_your_new_branch]``

5. start using the software as explained in the documentation
6. If there are updates, simply 
    
    - rebase your fork:
    
        **Step 1: Add the remote (original repo that you forked) and call it “upstream”**

        ``git remote add upstream https://gitlab.hzdr.de/kluget/run_and_event_viewer.git``

        **Step 2: Fetch all branches of remote upstream**

        ``git fetch upstream``

        **Step 3: Rewrite your main with upstream’s main/dev/nightly using git rebase.**

        ``git rebase upstream/[main/dev/nightly]`` 

    - pull the changes to your local machine:

        ``git pull``

Basic usage
-----------

The central class is :class:`.Experiment`. This class provides an object that can check for new runs and events, loads camera images, does basic processing steps and connects your data to metadata from an external google spreadsheet.

To get started, simply create an object of it:

.. code-block:: python
   
   import review as rv
   experiment = rv.Experiment()

The first thing you have to do is to define some settings. The most simple thing is to use one of the pre-configured set of settings. E.g., for European XFEL experiment 2818 there are three preconfigured sets: 

* ``settings_XFEL2818_share``: This can be used at Maxwell and directly communicates with the ``extra_mm`` API. Saves the output overview and tiff raw data to the ``usr/Shared`` directory. Careful! Should be run only by a single person at a time.
* ``settings_XFEL2818_user``: As before, but stores the outputs in subdirectories of the current working directory. Intended to be run from ``/usr/Software/[your name]`` - then it save save for usage and testing. 
* ``settings_XFEL2818_sftp``: This can be run anywhere in the world, retrieving data via SFTP from ``/usr/Shared/data` and storing the outputs in the current local working directory in subdirectories.

Example:

.. code-block:: python
   
	import review as rv
	import importlib
	importlib.reload(rv)

	# we have three predefined settings:
	#settings = rv.settings_XFEL2818_share # the instance running on Maxwell/online, generating the overview images and tiff files, careful!
	#settings = rv.settings_XFEL2818_user # an instance that only saves in the user subdirectory
	settings = rv.settings_XFEL2818_sftp # an instance for remote access via sftp

	metadata = rv.metadata_XFEL2818
	
	experiment = rv.Experiment(settings = settings, metadata = metadata)
	experiment.settings["username"] = "your_XFEL_username"
	experiment.settings["password"] = "your_XFEL_password" # optional - if you ommit this, you will be prompted to provide your password at runtime

A detailed explanation for all settings can be found :ref:`here <settings>`.

Now that's it! You can now check for new runs, filter for certain criteria, and open in imageJ, for example:

.. code-block:: python

   experiment.update() # looks for new Experiment and events
   experiment[27].open_in_imageJ() # opens all events of run 27 in an imageJ stack
   experiment["mainshots"].open_in_imageJ() # opens all mainshots in an imageJ stack
   hor, vert, hor_b, vert_b = experiment["mainshots"].get_lineprofiles() # returns the horizontal vertical profile and the corresponding backgrounds
   experiment.get_metadatas("delay") # returns a dictionary of run_numbers and delays, as set in a google spreadsheet
   
   filtered = Experiment(runs={k:v for k,v in experiment["mainshots"].runs.items() if v.get_metadata("delay") > 20}) # filter for certain criteria
   filtered.get_run_numbers()
   
   # get the last 10 mainshots, sorted by "delay"...
   experiment["mainshots"][-10].plot_profiles()
   # ...or by any other metadata key:
   experiment["mainshots"][-10:].plot_profiles(sort_by="I0")

   #... or sorted and filtered
   _limit = 10
   _sort_by = "delay"
   filtered = Experiment(Experiment={k:v for k,v in experiment[-_limit:].items() if v.get_metadata("delay") > 20}) # filter for certain criteria
   filtered_and_sorted = Experiment(runs=dict(sorted(filtered.get_metadatas(_sort_by).items(), key=lambda item: item[1]))) # you don't need to sort here, you could use "sort_by" in ``plot_profiles()``

   filtered_and_sorted.plot_profiles() # plots the profiles of the Experiment in a single graph
   
For the metadata lookup to work, you need to :ref:`connect a google spread sheet <google spreadsheet setup>` first. Over time, the metadata became quite interleafed with google spreadsheet. You actually really need a spreadsheet with metadata for each run. See the minimal `example spreadsheet <https://docs.google.com/spreadsheets/d/1K7JtCkNCRNVzbHzFCH_IQNWKpbyzReIOysysZKFmHPI/edit?usp=sharing>`__

A detailed explanation for all methods can be found :ref:`here <Classes>`. A detailed explanation for all plotting methods can be found :ref:`here <plotting>`. 

Run and event selection
-----------------------

The philosophy is that there exist three types of objects: :class:`.Experiment`, :class:`.Run`, :class:`.Event`. 
:class:`.Experiment` holds several runs, :class:`.Run` holds several events, :class:`.Event` is a single shot that provides the main functionality, such as :meth:`.Event.get_processed_image`, :meth:`.Event.plot_profile` etc.

To select a single run, just use the normal python syntax. For example, to select run 27:

.. code-block:: python

   experiment[27]
   
This returns an object of :class:`.Run`. For flat, i.e. continuous numbering, where each event gets a single new number, a run is defined based on the :ref:`labbook <google spreadsheet setup>` column "shot_type". 
The first preshot after a mainshot or postshot starts a new :class:`.Run` with ``run_number`` equal to the shot number. Then each following event is grouped to this run, each shot being a new :class:`.Event`
In the :class:`.Run` you can select a single :class:`.Event` again simply with the python syntax:

.. code-block:: python

   experiment[27][27]

This returns an object of type :class:`.Event`. You can also select the ``preshot``, ``mainshot``, or ``postshot``:

.. code-block:: python

   experiment[27]["mainshot"]

If there are more than one event of the given type, or if you select more than one event, an object of type :class:`.Run` is returned that only contains the selected methods, equivalently to if you select multiple runs, an object of type :class:`.Experiment` is returned:

.. code-block:: python

   # returns a Run object:
   experiment[27]["preshots"]
   experiment[27][27,28]
   experiment[27][27:29]
   experiment[27][-3:]
   
Normally, you don't have to care which type you get, because the high-level methods should do what you probably expect. As an example, look at the method ``plot_profiles``. If you call it for an :class:`.Event`, it will just plot a horizontal profile of that single shot at the marker position defined in the method call, the labbook, or center if none is given (for the standard detector). 
If you call the same method on a :class:`.Run` object, it will plot the profiles of all events in that run. And if you call it on an :class:`.Experiment` object, it plots all the profiles of all the events in all runs. 
The same applies for ``open_in_imageJ``, for example. 
There are a lot of plotting options, see :ref:`here <plotting>`. 

Filter and sort
---------------

You can filter and sort the runs with the usual python commands. For usability, a filter for shot_type is readily included:

.. code-block:: python

   import review as rv
   settings = rv.settings_XFEL2818_sftp # an instance for remote access via sftp
   metadata = rv.metadata_XFEL2818
   experiment = rv.Experiment(settings = settings, metadata = metadata)
   experiment.settings["username"] = "your_XFEL_username"
   experiment.settings["password"] = "your_XFEL_password"

   # 1. filter using python...
   filtered = Run(events={k:v for k,v in experiment[27].events.items() if v.shot_type == "mainshot"}) # filter for certain criteria
   print(filtered.get_run_numbers()

   # ...or equivalently:
   filtered = experiment[27]["mainshot"]
   print(filtered.get_run_numbers()

   # you can also ue that on Experiment:
   filtered = experiment["mainshot"]
   for run in filtered:
	   print(filtered.get_event_numbers()
	   
   # 2. filter for delay:
   filtered = Experiment(runs={k:v for k,v in experiment.runs.items() if v.get_metadata("delay") > 20}) # filter for certain criteria
   print(filtered.get_run_numbers()
   
   # 3 sort:
   sort_by = "delay"
   filtered_and_sorted = Experiment(runs=dict(sorted(filtered.get_metadatas(sort_by).items(), key=lambda item: item[1])))

.. note::
   The plotting methods already provide the "sort_by" keyword, where you can specify a metadata keyword to sort by (...or rather: they will in the future). Hence, you do not need to use the above syntax usually. 
