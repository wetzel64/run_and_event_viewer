.. _google spreadsheet setup:

Online labbook
==============
You can include a google spreadsheet as metadata source:

.. code-block:: python

    import review as rv
    experiment = rv.Experiment(verbose=False)
    experiment.metadata = rv.Metadata(keyfile = 'google_access.json', # The google API access token in json format
                                      sheet = 'Logbook EuXFEL 2818', # The name of the google sheet
                                      worksheet = 0, # the shotsheet number (0 is the first)
                                      vizsheet = 1, # the sheet where the vizualization parameters are stored
                                      head = 1 # number of leading header lines, EXcluding the unit line.
                                     )
									 
See the minimal `example spreadsheet <https://docs.google.com/spreadsheets/d/1K7JtCkNCRNVzbHzFCH_IQNWKpbyzReIOysysZKFmHPI/edit?usp=sharing>`__

For each single shot you need to create a new line in the spreadsheet, giving a new event- or run_number and defining a shot_type: ``preshot``, ``mainshot``, ``postshot`` or else. 