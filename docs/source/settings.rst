.. _Settings:

Settings
========

Those are the currently available settings:

.. code-block:: python

	import review as rv

	# settings for generator of overviews and data in share:
	settings={}
	settings["filePrefix"] = 'run_' # The string in front of the run_number, or, if there is none, one of '\' or '/' depending on your operating system
	# set eactly 1 to true:
	settings["local"] = False # set to True if you want to read the data from a local source
	settings["API"] = True # set to True to read from the mmm API
	settings["SFTP"] = False # set to True if you want to reat from SFTP

	settings["proposal"] = 2621 #2818

	settings["cache"] = True # store image copies locally
	settings["cacheMemory"] = 10 # store images in memory
	settings["remoteDataPath"] = "/gpfs/exfel/exp/HED/202201/p002818/usr/Shared/data/"
	settings["remoteOutputPath"] = "/gpfs/exfel/exp/HED/202201/p002818/usr/Shared/overviews/"
	settings["localDataPath"] = "/gpfs/exfel/exp/HED/202201/p002818/usr/Shared/data/" # only used for local=True or for cache
	settings["localOutputPath"] = "/gpfs/exfel/exp/HED/202201/p002818/usr/Shared/overviews/"

	settings["fileSeparator"] = "" # separator between run_number and event_number
	settings["fileType"] = "tif"
	settings["fileEnd"] = "-" # character string ending the numbers
	settings["fileMustHave"] = "JF3" # only consider images containing this string
	#for each detector, specify:
	settings["detector"] = "JF3" # The string defined here will be replaced by the detector string during runtime and defines the standard detector
	settings['threshold_lower_JF3'] = 6 # a lower value below which the detector values are set to settings['threshold_lower_JF3']. Jungfrau: [keV]; JF3: SAXS
	#settings['threshold_upper_JF3'] = 10 # an upper value below which the detector values are set to settings['threshold_upper_JF3']. Jungfrau: [keV]; JF3: SAXS
	settings["calibration_JF3"] = 8 # how much detector value is one unit?
	settings["unit_JF3"] = "photons" # unit string

	settings["imageNorm"] = "XFEL energy" # the key in the shotsheet where a normalizing amplitude is stored            

	settings["start_run"] = 290 # first run to consider
	settings["end_run"] = 299 # last run to consider

	settings["slack_token"]="xoxb-3172548063763-3174640001093-ezaj0pPuXFYkHcKn35U6Fd0S"
	settings_XFEL2818_share=deepcopy(settings)

	# settings for XFEL experiment in user directory on Maxwell:
	settings["remoteDataPath"] = "/gpfs/exfel/exp/HED/202201/p002818/usr/Shared/data/"
	settings["remoteOutputPath"] = "/gpfs/exfel/exp/HED/202201/p002818/usr/Shared/overviews/"
	settings["localDataPath"] = "data/" # only used for local=True or for cache
	settings["localOutputPath"] = "overviews/"
	settings_XFEL2818_user = deepcopy(settings)

	# settings for ssh to XFEL experiment
	settings["local"] = False # set to True if you want to read the data from a local source
	settings["API"] = False # set to True to read from the mmm API
	settings["SFTP"] = True # set to True if you want to reat from SFTP
	settings["hostname"] = "max-display.desy.de"
	settings["username"] = "Your_XFEL user name"
	settings["password"] = "Your XFEL password"
	settings_XFEL2818_sftp = deepcopy(settings)

	experiment = rv.Experiment( settings = settings_XFEL2818_sftp )
