.. _Classes:

Reference of classes and functions
==================================


The central class is :class:`.Experiment`. This class provides an object that can check for new Experiment and events, loads camera images, does basic processing steps and connects your data to metadata from an external google spreadsheet.  
It optionally uses the class :class:`.SSH` to retrieve Experiment and events from a remote server.   
A run is stored in the class :class:`.Run`, events are stored in class :class:`.Event`.  

Metadata can be optionally retrieved from a :ref:`google spreadsheet<google spreadsheet setup>` using class :class:`.Metadata`.

review.SSH()
------------
.. autoclass:: review.SSH
   :members:

review.Experiment()
-------------------
.. autoclass:: review.Experiment
   :members:
   :special-members:

review.Run()
------------
.. autoclass:: review.Run
   :members:
   :special-members:
   
review.Event()
--------------
.. autoclass:: review.Event
   :members:
   :special-members: