## EXTRA - M M M
## set of useful functions 
## summarized my M.Smid, based on some older work by many colleagues 

import glob as glob
import matplotlib.pyplot as plt
import numpy as np
import scipy.constants as sc
import scipy.signal as ss
from PIL import Image
import os
from scipy.signal import savgol_filter
from scipy import ndimage, misc, optimize
import extra_data

global intermediateDir
intermediateDir="./data"

diags=[]
diags.append(['JF4',"HED_IA1_JF500K4/DET/JNGFR04:daqOutput",0])
diags.append(['JF3',"HED_IA1_JF500K3/DET/JNGFR03:daqOutput",0])
diags.append(['JF2',"HED_IA1_JF500K2/DET/JNGFR02:daqOutput",0])
diags.append(['JF1',"HED_IA1_JF500K1/DET/JNGFR01:daqOutput",0])

diags.append(['Zyla',"HED_EXP_ZYLA/CAM/5:daqOutput",0])
diags.append(['Electrons',"HED_IA1_LT/CAM/CAM_3:daqOutput",0])
diags.append(['HIDG1',"HED_EXP_HIDG/CAM/CAM1:daqOutput",0])
diags.append(['HIDG2',"HED_EXP_HIDG/CAM/CAM2:daqOutput",0])
diags.append(['HIDG3',"HED_EXP_HIDG/CAM/CAM3:daqOutput",0])
diags.append(['ILM',"HED_IA1_LT/CAM/CAM_2:daqOutput",0])
diags.append(['IPM',"HED_OPT_IPM/ADC/1:channel_0.output",0])
diags.append(['ePix1',"HED_IA1_EPX100-1/DET/RECEIVER:daqOutput",0])
diags.append(['CRL_Z',"HED_IA1_NFS/MOTOR/CRL_Z",0])
diags.append(['XGM_HED',"HED_XTD6_XGM/XGM/DOOCS:output",0,'data.intensityTD'])
diags.append(['XGM_SA2',"SA2_XTD1_XGM/XGM/DOOCS:output",0,'data.intensityTD'])
diags.append(['LC_AXIS_4',"HED_IA1_DETM/MOTOR/LC_AXIS_4",0])


global ePix1_dark
ePix1_dark=[]

def version():
    print("extra_mmm 18.3.2022")

def ImSave(data,name,debug=0):
    array = np.array(data)
    if debug:
        print(np.shape(array))
        print(type(array))
        try:
            dummy = Image.fromarray(array)
            print(np.shape(dummy))
            print(type(dummy))
            dummy.save(name)  
        except Exception as e: # work on python 3.x
            print(str(e))    
            print("Some error while saving...")
            
            
    else:
        dummy = Image.fromarray(array)
        dummy.save(name)  
        

def open_run(proposal, runNo, data='proc'):
    try:
        run = extra_data.open_run(proposal, runNo, data=data)  #Opening the given run
        return run
    except Exception as e: # work on python 3.x
        return 0


def get_motor_values(run,nickname):
    motor=get_fullname(nickname)
    posMotor=run.get_array(motor,"actualPosition.value")
    return posMotor.values

def get_diag_trainIds(run,nickname):
    motor=get_fullname(nickname)
    if "actualPosition.value" in run.keys_for_source(motor):
        posMotor=run.get_array(motor,"actualPosition.value")
        trainIds = posMotor.coords['trainId']
    elif "data.trainId" in run.keys_for_source(motor):
        trains=run.get_array(motor,"data.trainId")
        trainIds = trains.values
    else:
        print('not sure where to take trainIds..')
        
    return trainIds


def get_shot_I(run,trainId=None,threshold=99e9,debug=0,average=True):
    #IPM2 = run['HED_OPT_IPM/ADC/1:channel_2.output','data.peaks'].xarray()
    xgm,xgm_trains=get_array_raw(run,'XGM_SA2')  #the values are hopefully in μJ
    xgm=xgm[:,0]
#    try:
 #       aa=IPM2.values[:,0]
  #  except:
   #     aa=IPM2.values[:]
    if trainId == None:
        if average:
            trainId = -1
        else:
            trainId = get_shot_trainId(run)
    else:
        if trainId >= 0:
            # only a single shot is requested. get the position of the trainId in the array:
            sel=np.argmax(xgm_trains==trainId)
            if sel==0:
                print('This train did not arrive.')
                return 0
            if debug:
                print(trainId)
#                print(xgm_trains)
                print('found the train at postions:')
                print(sel)
            return xgm[sel]
#            for sel, id_ in enumerate(xgm_trains):
 #               if id_.trainId.data == trainId:
  #                  break
            # get the intensity of the selected shot:
   #         try:
#                Is=(IPM2.values[sel,0])
#            except:
 #               Is=(IPM2.values[sel])
  #          return -Is
        else:
            # select all shots below the threshold:
            sel=xgm<threshold
            Is=(xgm[sel])
            if average: 
                return np.mean(Is)
            else:
                return np.sum(Is)

def get_shot_trainId(run,debug=0):
    """
    Returns the trainId of train when the laser was shot, based on IPM.

    This is as it worked on proposal 2621. Should be still made more stable and general.    
    """
    if not "HED_OPT_IPM/ADC/1" in run.all_sources:
        print("no IPM data!!!")
        return 0
    IPM2 = run['HED_OPT_IPM/ADC/1:channel_2.output','data.peaks'].xarray()
    aa=IPM2.values[:,0]
    ids=np.argmin(aa)
    shotTrainId=IPM2.trainId[ids].trainId.data
    if debug:
        print(np.shape(IPM2.values))
        print(aa)
        print(ids)
        plt.figure()
        plt.plot(aa)
        print(shotTrainId)
    return shotTrainId

def get_shot_trainIds(run,threshold=-60,debug=0):
    """
    Returns an array of trainIds when the laser was shot, based on IPM. It could work if there were multiple shots, or if the diagnostics is not working properly and we are not sure which is the real one..

    This is as it worked on proposal 2621. Should be still made more stable and general.    
    """    
    if not "HED_OPT_IPM/ADC/1" in run.all_sources:
        print("no IPM data!!!")
        return 0
    IPM2 = run['HED_OPT_IPM/ADC/1:channel_2.output','data.peaks'].xarray()
    aa=IPM2.values[:,0]
    sel=aa<threshold
    Ids=(IPM2.trainId[sel])
    if debug:
#        print(np.shape(IPM2.values))
 #       print(aa)
#        print(ids)
        plt.figure()
        plt.plot(aa)
        plt.plot(aa*0+threshold)
#        print(shotTrainId)
    return Ids.values
        
def get_fullname(nickname):
    diag=''
    for d in diags:
        if d[0]==nickname:
            diag=d[1]
    if diag=='':
        print('diag_nickname not in dictionary!')
    return diag
        
def get_array(run,runNo,diag_nickname,trainId=-1,diag='',recalc=0,threshold_lower=-1,threshold_upper=-1,debug=0,average=True, fn_prefix=""):
    """ 
    Similar like get image, just more general for any arrays

    :param trainId: (*int*) *optional* - set this to the trainId of the shot you want to obtain, or ``-1`` to get all shots averaged or summed
    :param average: (*bool*) *optional* - set this to ``True`` (default) to average in case of a train series (``trainId==-1``) or to ``False`` to sum the trains.
    """
    global intermediateDir    
    ss=''
    if diag=='':
        for d in diags:
            if d[0]==diag_nickname:
                diag=d[1]
                if trainId!=-1:
                    trainId=trainId+d[2]
        if diag=='':
            print('diag_nickname not in dictionary!')
            return []
    if trainId>=0:
        ss='_trainId{:03.0f}'.format(trainId)
    else:
        ss='_mean'

    fn=intermediateDir+'/'+fn_prefix+'%04d-'%(runNo)+diag_nickname+'%s.tif'%(ss)
    if debug:
        print(fn)
        print(diag)
        print(trainId)
    if recalc:
        image=read_and_process_images(run,diag,trainId,threshold_lower,threshold_upper,debug,average=average)        
        if debug:
            print(np.shape(image))
            print(type(image))
        if image.size!=1:
            ImSave(image,fn,debug=debug)
        else:
            image=np.array([])
            print("No Data!! (a)"+diag_nickname)    
    else:
        if os.path.isfile(fn):
            jungf = Image.open(fn)
            image = np.array(jungf)
            if debug:
                print("Data loaded. "+diag_nickname)
        else:
            image=read_and_process_images(run,diag,trainId,threshold_lower,threshold_upper,debug,average=average)
            if image.size!=1:
                ImSave(image,fn,debug=debug)
            else:
                image=np.array([])
                print("No data (b)"+diag_nickname)    
    return image
        
    
    
    
def get_array_raw(run,diag_nickname,diag=''):
    key='data.image.pixels'
    if diag=='':
        for d in diags:
            if d[0]==diag_nickname:
                diag=d[1]
                if np.size(d)>3:
                    key=d[3]
        if diag=='':
            print('diag_nickname not in dictionary!')
            return []    
    
#    array = run.get_array('HED_IA1_EPX100-1/DET/RECEIVER:daqOutput','data.image.pixels').values
    if 'JF' in diag:
        images= run[diag, 'data.adc'].xarray()
        trains=images.trainId.values
        values=np.squeeze(images[:,0,:,:]).values
    else:
#        images= run[diag, key].xarray()           
        images=run.get_array(diag,key)
        trains=images.trainId.values
        values=images.values
    return values,trains
        
def get_image(run,runNo,diag_nickname,trainId=-1,average=True,diag='',recalc=0,threshold_lower=-1,threshold_upper=-1,debug=0,save=True, fn_prefix=""):
    """ 
    This is truly the main function of the librarly, hopefully properly documented on wiki on Gitlab.
    
    :param trainId: (*int*) *optional* - set this to the trainId of the shot you want to obtain, or ``-1`` to get all shots averaged or summed
    :param average: (*bool*) *optional* - set this to ``True`` (default) to average in case of a train series (``trainId==-1``) or to ``False`` to sum the trains.
    """
    global intermediateDir    
    ss=''
    if diag=='':
        for d in diags:
            if d[0]==diag_nickname:
                diag=d[1]
                if trainId!=-1:
                    trainId=trainId+d[2]
        if diag=='':
            print('diag_nickname not in dictionary!' + diag_nickname)
            return []
    if trainId>=0:
        ss='_trainId{:03.0f}'.format(trainId)
    else:
        ss='_mean'

    fn=intermediateDir+'/'+fn_prefix+'%04d-'%(runNo)+diag_nickname+'%s.tif'%(ss)
    if debug:
        print(fn)
        print(diag)
        print("a",trainId)
    if recalc:
        image=read_and_process_images(run,diag,trainId,threshold_lower,threshold_upper,debug,average=average)   
        if debug:
            print("b",np.shape(image))
            print(type(image))
        if save:
            if image.size!=1:
                ImSave(image,fn,debug=debug)
            else:
                image=np.array([])
                print("No Data!! (a)"+diag_nickname)    
    else:
        if os.path.isfile(fn):
            jungf = Image.open(fn)
            image = np.array(jungf)
            if debug:
                print("Data loaded. "+diag_nickname)
        else:
            image=read_and_process_images(run,diag,trainId,threshold_lower,threshold_upper,debug,average=average)
            if save:
                if image.size!=1:
                    ImSave(image,fn,debug=debug)
                else:
                    image=np.array([])
                    print("No data (b)"+diag_nickname)    
    return image

def read_and_process_array(run,diag,trainId=-1,average=True,threshold_lower=-1,threshold_upper=-1,debug=0):
    """ 
    Subfunction of get_image

    :param trainId: (*int*) *optional* - set this to the trainId of the shot you want to obtain, or ``-1`` to get all shots averaged or summed
    :param average: (*bool*) *optional* - set this to ``True`` (default) to average in case of a train series (``trainId==-1``) or to ``False`` to sum the trains.
    """
    if not diag in run.all_sources:
        if debug:
            print(diag+" not in all_sources.")
        return np.array([0])
    if 'JF' in diag:
        images= run[diag, 'data.adc'].xarray()
        images=np.squeeze(images[:,0,:,:])
    else:
        images= run[diag, 'data.image.pixels'].xarray()           
    number_of_images=np.shape(images)[0]
    if number_of_images==0:
        if debug:
            print(diag+" no images in run.")
        return np.array([0])
    if debug:
        print("%d images in run"%(number_of_images))
    if trainId>0:
        image=images.data[images.trainId.data==trainId]
        image=np.squeeze(image)
        if type(image)!=np.ndarray:
            image=image.values
        if threshold_lower>0:
            fl=image<threshold_lower
            image[fl]=0
        if threshold_upper>0:
            fl=image>threshold_upper
            image[fl]=0
    else:        
        images=images.values
        if threshold_lower>0:
            fl=images<threshold_lower
            images[fl]=0
        if threshold_upper>0:
            fl=images>threshold_upper
            images[fl]=0 
        if average:
            image = np.mean(images,axis=0)
        else:
            image = np.sum(images,axis=0)
        image = np.squeeze(image)
    if np.shape(image)[0]==0:
        print(" empty image array")
        return np.array([0])        
    return image
    
    
    
    
def read_and_process_images(run,diag,trainId=-1,threshold_lower=-1,threshold_upper=-1,debug=0, average=True):
    """ 
    Subfunction of get_image

    :param trainId: (*int*) *optional* - set this to the trainId of the shot you want to obtain, or ``-1`` to get all shots averaged or summed
    :param average: (*bool*) *optional* - set this to ``True`` (default) to average in case of a train series (``trainId==-1``) or to ``False`` to sum the trains.
    """
    if not diag in run.all_sources:
        if debug:
            print(diag+" not in all_sources.")
        return np.array([0])
    if 'JF' in diag:
        images= run[diag, 'data.adc'].xarray()
        images=np.squeeze(images[:,0,:,:])
    else:
        images= run[diag, 'data.image.pixels'].xarray()           
    number_of_images=np.shape(images)[0]
    if number_of_images==0:
        if debug:
            print(diag+" no images in run.")
        return np.array([0])
    if debug:
        print("%d images in run"%(number_of_images))
    darkframe=[]
    if 'EPX100-1' in diag:
        global ePix1_dark
        darkframe=ePix1_dark
        
    if trainId>0:
        image=images.data[images.trainId.data==trainId]
        image=np.squeeze(image)
        if np.size(darkframe)>0:
            image=image-darkframe
        if type(image)!=np.ndarray:
            image=image.values
        if threshold_lower>0:
            fl=image<threshold_lower
            image[fl]=0
        if threshold_upper>0:
            fl=image>threshold_upper
            image[fl]=0
    else:        
        images=images.values
        if np.size(darkframe)>0:
            images=images-darkframe
        
        if threshold_lower>0:
            fl=images<threshold_lower
            images[fl]=0
        if threshold_upper>0:
            fl=images>threshold_upper
            images[fl]=0     
        if average:
            image = np.mean(images,axis=0)       
        else:
            image = np.sum(images,axis=0)       
        image = np.squeeze(image)
    if np.shape(image)[0]==0:
        print(" empty image array")
        return np.array([0])        
    return image
